﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Proyeco1_IPC2_.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; REGISTRO DE EMPLEADO</h1>
        <p class="lead">Registra empleados nuevos para que comiencen a efectuar ventas, administren sus pedidos de mejor manera, y lleven un mejor control del cumplimiento de sus metas, tanto mensuales como anuales.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <p>
                &nbsp;</p>
            
            
        </div>
        <div class="col-md-4">
            <h3>Ingresa tus datos de registro</h3>
            <h5>NIT empleado &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                <asp:TextBox ID="boxnit" runat="server" Width="173px"></asp:TextBox>
            </h5>
            <h5>Contraseña*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="boxcontra" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </h5>
            <h5>Confirmar contraseña*&nbsp; <asp:TextBox ID="boxconfirmacion" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </h5>
            
          
            <p>
                
                <asp:Button ID="Registro" runat="server" CssClass="btn-default" Text="Registrarme" OnClick="Registro_Click" />
                <asp:Label ID="Mensajes" runat="server" ></asp:Label>
            </p>
        </div>
        <div class="col-md-4">
            <h3>Datos de autorización</h3>
            <p>
                Para poder registrarte y tener el acceso a la plataforma, tus datos ya deben estar en la plataforma, por este medio no te estas registrando como empleado, aqui solo estableces tu contraseña, y tu usuario sera siempre tu nit </p>
        </div>
    </div>
</asp:Content>
