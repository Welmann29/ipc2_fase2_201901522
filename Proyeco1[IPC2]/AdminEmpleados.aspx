﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminEmpleados.aspx.cs" Inherits="Proyeco1_IPC2_.AdminEmpleados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>GERENTES</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--AQUI SE VERAN LOS GERENTES-->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>JOSE PEREZ</h3>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Mas detalles &raquo;</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--AQUI SE VERAN LOS GERENTES-->

    <!--CABECERA SUPERVISORES-->
    <div class="city">
        <h1>SUPERVISORES</h1>
    </div>
    <!--FIN CABECERA SUPERVISORES-->

    <!--AQUI SE VERAN LOS SUPERVISORES-->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>JOSE PEREZ</h3>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT SUPERIOR:</label>
                    </div>
                    <div class="form-group">
                        <P>###7895621###</P>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Mas detalles &raquo;</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--AQUI SE VERAN LOS SUPERVISORES-->

    <!--CABECERA VENDEDORES-->
    <div class="city">
        <h1>VENDEDORES</h1>
    </div>
    <!--FIN CABECERA VENDEDORES-->

    <!--AQUI SE VERAN LOS VENDEDORES-->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>JOSE PEREZ</h3>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT SUPERIOR:</label>
                    </div>
                    <div class="form-group">
                        <P>###7895621###</P>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Mas detalles &raquo;</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--AQUI SE VERAN LOS VENDEDORES-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
