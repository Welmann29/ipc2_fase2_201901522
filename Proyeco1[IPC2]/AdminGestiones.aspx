﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminGestiones.aspx.cs" Inherits="Proyeco1_IPC2_.AdminGestiones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>GESTIONES GENERALES</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--CUERPO DE LA PAGINA-->
    <section class="content">
        <div class="row">
            <!--Asignar lista de precios vigente-->
            <div class="col-md-4">
                <div>
                    <label>
                        ASIGNACION DE LISTA VIGENTE 
                    </label>
                </div>

                <div>
                    <label>
                         Listas disponibles: 
                    </label>
                </div>

                <div align="center">
                    <p>

                        <asp:DropDownList ID="ListasPrecios" runat="server">
                        </asp:DropDownList>

                    </p>
                </div>
                <div align="center">
                    <p>

                        <asp:Button ID="SeleccionadorLista" runat="server" CssClass="btn btn-danger" Text="Seleccionar lista" OnClick="SeleccionadorLista_Click" />

                    </p>
                </div>
                <div align="center">
                    <label>

                    <asp:Label ID="AdvertenciasListaVigente" runat="server" ></asp:Label>

                    </label>
                </div>
            </div>
            <!--Asignar lista de precios vigente-->

            <!--Carga de archivos XML-->
            <div class="col-md-4">
                <div>
                    <label>
                        CARGA DE ARCHIVOS XML
                    </label>
                </div>
                <div>
                    <p>
                        Carga todo a tu base de datos por aqui, debes colocar los archivos en la carpeta, ArchivosXML, al lado de la solucion,
                        lugo presiona el siguiente boton para cargar los archivos
                    </p>
                </div>
                
                <div align="center">
                    <p>

                        <asp:Button ID="CargaXML" runat="server" CssClass="btn btn-danger" Text="Cargar Archivos XML" OnClick="CargaXML_Click" />

                    </p>
                </div>
                <div align="center">
                    <label>

                    <asp:Label ID="AdvertenciasCargas" runat="server" ></asp:Label>

                    </label>
                </div>
            </div>
            <!--Carga de archivos XML-->
        </div>
    </section>
    <!--CUERPO DE LA PAGINA-->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
