﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Proyeco1_IPC2_
{
    public partial class AdminGestiones : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public Inserciones Inserciones = new Inserciones();
        protected void Page_Load(object sender, EventArgs e)
        {
            string listasDePrecios = "SELECT CodigoLista, NombreLista FROM LISTAPRECIO";

            if (!IsPostBack)
            {
                ListasPrecios.DataSource = Dset(listasDePrecios);
                ListasPrecios.DataMember = "datos";
                ListasPrecios.DataTextField = "NombreLista";
                ListasPrecios.DataValueField = "CodigoLista";
                ListasPrecios.DataBind();

                
            }

        }

        protected DataSet Dset(string sentencia)
        {

            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }
        public string ruta = "C://Users//welma//OneDrive//Escritorio//Nueva carpeta//ipc2_fase2_201901522//DocumentosXML//";
        public string entrada = "definicion/";

        protected void lecturaCategorias()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Categorias.xml");

            XmlNodeList listaCategorias = doc.SelectNodes(entrada + "categoria");

            XmlNode categoria;

            for (int i = 0; i < listaCategorias.Count; i++) {
                categoria = listaCategorias.Item(i);

                int id = Int32.Parse(categoria.SelectSingleNode("codigo").InnerText);
                string nombre = categoria.SelectSingleNode("nombre").InnerText;
                //Aqui colocar insersion
                Inserciones.CategoriaProducto(id, nombre);
                Console.Write("Id: " + id + " nombre: " + nombre );

            }
        }

        protected void lecturaProductos() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Productos.xml");

            XmlNodeList listaProductos = doc.SelectNodes(entrada + "producto");

            XmlNode producto;

            for (int i = 0; i < listaProductos.Count; i++)
            {
                producto = listaProductos.Item(i);

                int id = Int32.Parse(producto.SelectSingleNode("codigo").InnerText);
                string nombre = producto.SelectSingleNode("nombre").InnerText;
                string descripcion = producto.SelectSingleNode("descripcion").InnerText;
                int categoria = Int32.Parse(producto.SelectSingleNode("categoria").InnerText);
                //Aqui colocar incersion
                Inserciones.Producto(id, nombre, descripcion, categoria);
                Console.Write("Id: " + id + " nombre: " + nombre);

            }

        }

        protected void lecturaListas() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Listas.xml");

            XmlNodeList listaListas = doc.SelectNodes(entrada + "lista");

            XmlNode lista;

            for (int i = 0; i < listaListas.Count; i++)
            {
                lista = listaListas.Item(i);

                int id = Int32.Parse(lista.SelectSingleNode("codigo").InnerText);
                string nombre = lista.SelectSingleNode("nombre").InnerText;
                string FechaInicio = lista.SelectSingleNode("fecha_inicio").InnerText;
                string FechaFin = lista.SelectSingleNode("fecha_fin").InnerText;
                DateTime Fecha = DateTime.Parse(FechaInicio);
                FechaInicio = Fecha.ToString("yyyy/MM/dd");
                Fecha = DateTime.Parse(FechaFin);
                FechaFin = Fecha.ToString("yyyy/MM/dd");
                //Aqui colocar incersion
                Console.Write("Id: " + id + " nombre: " + nombre);
                Inserciones.Lista(id, nombre, FechaInicio, FechaFin);
                
                XmlNodeList items = lista.SelectSingleNode("detalle").SelectNodes("item");
                XmlNode item;
                int holi = items.Count;
                for (int j = 0; j < items.Count; j++) {
                    item = items.Item(j);
                    int codigoProducto = Int32.Parse(item.SelectSingleNode("codigo_producto").InnerText);
                    float precio = float.Parse(item.SelectSingleNode("valor").InnerText);
                    //Aqui colocar incersion
                    Console.Write("Id: " + id + " nombre: " + nombre);
                    Inserciones.AsignacionPrecio(id, codigoProducto, precio);
                }

            }
        }

        protected void lecturaDepartamentos() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Departamentos.xml");

            XmlNodeList listaDepartamentos = doc.SelectNodes(entrada + "depto");

            XmlNode departamento;

            for (int i = 0; i < listaDepartamentos.Count; i++) {
                departamento = listaDepartamentos.Item(i);

                int codigo = Int32.Parse(departamento.SelectSingleNode("codigo").InnerText);
                string nombre = departamento.SelectSingleNode("nombre").InnerText;
                Console.WriteLine("joputa");
                //Insertar metida a sql
                Inserciones.Departamento(codigo, nombre);
            }
        }

        protected void lecturaMunicipios() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Municipios.xml");

            XmlNodeList listaMunicipios = doc.SelectNodes(entrada + "ciudad");

            XmlNode municipio;

            for (int i = 0; i < listaMunicipios.Count; i++)
            {
                municipio = listaMunicipios.Item(i);

                int codigo = Int32.Parse(municipio.SelectSingleNode("codigo").InnerText);
                string nombre = municipio.SelectSingleNode("nombre").InnerText;
                int codigoCiudad = Int32.Parse(municipio.SelectSingleNode("codigo_departamento").InnerText);
                Console.WriteLine("joputa");
                //Insertar metida a sql
                Inserciones.Municipio(codigo, nombre, codigoCiudad);
            }
        }


        protected void lecturaCliente() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Clientes.xml");

            XmlNodeList listaClientes = doc.SelectNodes(entrada + "cliente");

            XmlNode cliente;

            for (int i = 0; i < listaClientes.Count; i++) {
                cliente = listaClientes.Item(i);

                string nit = cliente.SelectSingleNode("NIT").InnerText;
                string nombres = cliente.SelectSingleNode("nombres").InnerText;
                string apellidos = cliente.SelectSingleNode("apellidos").InnerText;
                string nacimiento = cliente.SelectSingleNode("nacimiento").InnerText;
                string direccion = cliente.SelectSingleNode("direccion").InnerText;
                string telefono = cliente.SelectSingleNode("telefono").InnerText;
                string celular = cliente.SelectSingleNode("celular").InnerText;
                string email = cliente.SelectSingleNode("email").InnerText;
                int ciudad = Int32.Parse(cliente.SelectSingleNode("ciudad").InnerText);
                int depto = Int32.Parse(cliente.SelectSingleNode("depto").InnerText);
                float limiteCredito = float.Parse(cliente.SelectSingleNode("limite_credito").InnerText);
                int diasCredito = Int32.Parse(cliente.SelectSingleNode("días_credito").InnerText);
                DateTime Fnacimiento = DateTime.Parse(nacimiento);
                nacimiento = Fnacimiento.ToString("yyyy/MM/dd");

                //Insercion a la base de datos 
                Console.WriteLine("holi");
                Inserciones.Cliente(nit, nombres, apellidos, nacimiento, direccion, telefono, celular, email, depto, ciudad, limiteCredito, diasCredito, 68);
            }
        }

        protected void lecturaEmpleado() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Empleados.xml");

            XmlNodeList listaEmpleados = doc.SelectNodes(entrada + "empleado");

            XmlNode empleado;

            for (int i = 0; i < listaEmpleados.Count; i++)
            {
                empleado = listaEmpleados.Item(i);

                string nit = empleado.SelectSingleNode("NIT").InnerText;
                string nombres = empleado.SelectSingleNode("nombres").InnerText;
                string apellidos = empleado.SelectSingleNode("apellidos").InnerText;
                string nacimiento = empleado.SelectSingleNode("nacimiento").InnerText;
                string direccion = empleado.SelectSingleNode("direccion").InnerText;
                string telefono = empleado.SelectSingleNode("telefono").InnerText;
                string celular = empleado.SelectSingleNode("celular").InnerText;
                string email = empleado.SelectSingleNode("email").InnerText;
                int codigoPuesto = Int32.Parse(empleado.SelectSingleNode("codigo_puesto").InnerText);
                string NITSuperior = empleado.SelectSingleNode("codigo_jefe").InnerText;        
                string contra = empleado.SelectSingleNode("pass").InnerText;
                DateTime Fnacimiento = DateTime.Parse(nacimiento);
                nacimiento = Fnacimiento.ToString("yyyy/MM/dd");
                if (NITSuperior.Equals("")) {
                    NITSuperior = "NULL";
                }

                Inserciones.Empleado(nit, NITSuperior, nombres, apellidos, direccion, telefono, celular, email, codigoPuesto, nacimiento, contra);
                //Insercion a la base de datos 
                Console.WriteLine("holi");
            }
        }

        protected int ObtencionIdMeta(string fecha, string nit)
        {
            int IdMeta = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", nit);

            SqlCommand comando = new SqlCommand("SELECT IdMeta FROM METAMES WHERE METAMES.NITEmpleado = @nit AND FechaInicio = '" + fecha + "'", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    IdMeta = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return IdMeta;
        }
        protected void lecturaMetas() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Metas.xml");

            XmlNodeList listaMetas = doc.SelectNodes(entrada + "meta");

            XmlNode meta;
            for (int i = 0; i < listaMetas.Count; i++)
            {
                meta = listaMetas.Item(i);

                string NITEmpleado = meta.SelectSingleNode("NIT_empleado").InnerText;
                string FechaMeta = meta.SelectSingleNode("mes_meta").InnerText;
                DateTime fmeta = DateTime.Parse(FechaMeta);
                FechaMeta = fmeta.ToString("yyyy/MM/dd");

                Inserciones.GeneracionMeta(NITEmpleado, FechaMeta);
                //Aqui colocar incersion
                Console.Write("Id:  nombre: ");
                int IdMeta = ObtencionIdMeta(FechaMeta, NITEmpleado);


                XmlNodeList items = meta.SelectSingleNode("detalle").SelectNodes("item");
                XmlNode item;
                int holi = items.Count;
                for (int j = 0; j < items.Count; j++)
                {
                    item = items.Item(j);
                    int codigoCategoria = Int32.Parse(item.SelectSingleNode("codigo_producto").InnerText);
                    float metaCategoria = float.Parse(item.SelectSingleNode("meta_venta").InnerText);
                    Inserciones.MetaCategoria(codigoCategoria, IdMeta, metaCategoria);
                    //Aqui colocar incersion
                    Console.Write("Id:  nombre: ");
                }

            }

        }

        protected void lecturaMoneda() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "Monedas.xml");

            XmlNodeList listaMonedas = doc.SelectNodes(entrada + "moneda");

            XmlNode moneda;

            for (int i = 0; i < listaMonedas.Count; i++)
            {
                moneda = listaMonedas.Item(i);

                string nombre = moneda.SelectSingleNode("nombre").InnerText;
                string simbolo = moneda.SelectSingleNode("simbolo").InnerText;
                float tasa = float.Parse(moneda.SelectSingleNode("tasa").InnerText);
                Console.WriteLine("joputa");
                //Insertar metida a sql
                Inserciones.Monedas(nombre, simbolo, tasa);
            }
        }

        protected void lecturaListaxCliente() {
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta + "LstxCliente.xml");

            XmlNodeList listasClientes = doc.SelectNodes(entrada + "lstXCliente");

            XmlNode listaCliente;

            for (int i = 0; i < listasClientes.Count; i++)
            {
                listaCliente = listasClientes.Item(i);

                string nit = listaCliente.SelectSingleNode("cliente").InnerText;
                
                int lista = Int32.Parse(listaCliente.SelectSingleNode("codigo_lista").InnerText);
                Console.WriteLine("joputa");
                //Insertar metida a sql
                Inserciones.ListaxCliente(nit, lista);
            }

        }


        protected void CargaXML_Click(object sender, EventArgs e)
        {
            lecturaMoneda();
            lecturaCategorias();
            lecturaProductos();
            lecturaListas();
            lecturaDepartamentos();
            lecturaMunicipios();
            lecturaCliente();
            lecturaEmpleado();
            lecturaMetas();
            
            lecturaListaxCliente();
            AdvertenciasCargas.Text = "Carga exitosa";
        }

        protected void SeleccionadorLista_Click(object sender, EventArgs e)
        {
            Inserciones.AsignacionListaVigente(Int32.Parse(ListasPrecios.SelectedValue));
            AdvertenciasListaVigente.Text = "Lista Vigente asignada";
        }
    }
}