﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminMetas.aspx.cs" Inherits="Proyeco1_IPC2_.AdminMetas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>ASIGNACION DE METAS</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--AQUI SE VERAN LAS OPCIONES DE ASIGNACION DE METAS-->
    <section class="content">
        <div class="row">
            <!--PARA CENTAR LA OTRA-->
            <div class="col-md-4">
                <div class="form-group">
                        <h3>GENERACION META DE VENTA</h3>
                </div>
                <div class="form-group">
                        <p>Una vez generes la meta debes asignar la meta para cada categoria</p>
                </div>
                <div class="form-group">
                        <label>AÑO Y MES</label>
                 </div>
                <div align="center">

                    <asp:DropDownList ID="MES" runat="server">
                        <asp:ListItem Value="01">Enero</asp:ListItem>
                        <asp:ListItem Value="02">Febrero</asp:ListItem>
                        <asp:ListItem Value="03">Marzo</asp:ListItem>
                        <asp:ListItem Value="04">Abril</asp:ListItem>
                        <asp:ListItem Value="05">Mayo</asp:ListItem>
                        <asp:ListItem Value="06">Junio</asp:ListItem>
                        <asp:ListItem Value="07">Julio</asp:ListItem>
                        <asp:ListItem Value="08">Agosto</asp:ListItem>
                        <asp:ListItem Value="09">Septiembre</asp:ListItem>
                        <asp:ListItem Value="10">Octubre</asp:ListItem>
                        <asp:ListItem Value="11">Noviembre</asp:ListItem>
                        <asp:ListItem Value="12">Diciembre</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="AÑO" runat="server">
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                        <asp:ListItem>2022</asp:ListItem>
                    </asp:DropDownList>

                </div>
                <div>
                    <label>
                        VENDEDOR
                    </label>
                </div>
                <div align="center">
                    <p>

                        <asp:DropDownList ID="vendedores" runat="server">
                        </asp:DropDownList>

                    </p>
                </div>
                <div align="center">
                    <label>

                    <asp:Button ID="GeneracionMeta" runat="server" CssClass="btn btn-danger" Text="GenerarMeta" OnClick="GeneracionMeta_Click" />

                    </label>
                </div>
                <div>
                    <label>

                    <asp:Label ID="AdvertenciasGeneracion" runat="server"></asp:Label>

                    </label>
                </div>
            </div>
            <!--PARA CENTRAR LA OTRA -->

            <!--ASIGNACION A SUPERVISORES-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>ASIGNACION META DE VENTAS</h3>
                    </div>
                    <div class="form-group">
                        <p>Aqui puedes asignar una meta de ventas a los vendedores</p>
                    </div>
                    <div class="form-group">
                        <label>CATEGORIA</label>
                    </div>
                    <div class="form-group" align="center">
                        
                        <asp:DropDownList ID="Categoria" runat="server">
                        </asp:DropDownList>
                        
                    </div>
                    
                    <div class="form-group">
                        <label>META DE VENTAS EN DOLARES</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="boxmeta" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div align="center">

                        <asp:Button ID="Asignar" runat="server" CssClass="btn btn-danger" Text="Asignar" OnClick="Asignar_Click" />

                    </div>   
                    <div>
                        <label>
                            <asp:Label ID="AdvertenciasAsignacion" runat="server" ></asp:Label>
                            
                        </label>
                    </div>
                </div>
            </div>
            <!--ASIGNACION A SUPERVISORES FIN-->
        </div>
    </section>
    <!--AQUI SE VERAN LAS OPCIONES DE ASIGNACION DE METAS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
