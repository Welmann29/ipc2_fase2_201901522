﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class AdminMetas : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        Inserciones inserciones = new Inserciones();

        protected void Page_Load(object sender, EventArgs e)
        {
            string vendedorescon = "SELECT Nombres + ' ' + Apellidos as NombreCompleto, NITEmpleado FROM EMPLEADO WHERE TipoEmpleado = 1";
            string categoria = "SELECT * FROM CATEGORIA";

            if (!IsPostBack)
            {
                vendedores.DataSource = Dset(vendedorescon);
                vendedores.DataMember = "datos";
                vendedores.DataTextField = "NombreCompleto";
                vendedores.DataValueField = "NITEmpleado";
                vendedores.DataBind();

                Categoria.DataSource = Dset(categoria);
                Categoria.DataMember = "datos";
                Categoria.DataTextField = "NombreCategoria";
                Categoria.DataValueField = "IdCategoria";
                Categoria.DataBind();
            }

        }
        protected DataSet Dset(string sentencia)
        {

            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected int ObtencionIdMeta(string mes, string año, string nit)
        {
            int IdMeta = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", nit);

            SqlCommand comando = new SqlCommand("SELECT IdMeta FROM METAMES WHERE METAMES.NITEmpleado = @nit AND FechaInicio = '" + año + "/" + mes + "/01'", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    IdMeta = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return IdMeta;
        }

        protected int ObtencionIdMetaxCategoria(int IdMeta, int categoria)
        {
            int IdMetaCategoria = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@meta", IdMeta);

            SqlCommand comando = new SqlCommand("SELECT IdMetaCategoria FROM METACATEGORIA WHERE IdMeta = @meta AND IdCategoria = " + categoria, sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    IdMetaCategoria = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return IdMetaCategoria;
        }

        protected void GeneracionMeta_Click(object sender, EventArgs e)
        {
            if (ObtencionIdMeta(MES.SelectedValue, AÑO.SelectedValue, vendedores.SelectedValue) == 0) {
                string fechaInicio = AÑO.SelectedValue + "/" + MES.SelectedValue + "/01";
                inserciones.GeneracionMeta(vendedores.SelectedValue, fechaInicio);
                AdvertenciasGeneracion.Text = "Meta generada, asigne la meta por categoria";
            }
            else {
                AdvertenciasGeneracion.Text = "La meta de este periodo para este vendedor ya fue generada, asignale el monto por categoria";
            }
        }

        protected void Asignar_Click(object sender, EventArgs e)
        {
            int IdMeta = ObtencionIdMeta(MES.SelectedValue, AÑO.SelectedValue, vendedores.SelectedValue);
            if (IdMeta != 0) {
                if (ObtencionIdMetaxCategoria(IdMeta, Int32.Parse(Categoria.SelectedValue)) == 0) {
                    inserciones.MetaCategoria(Int32.Parse(Categoria.SelectedValue), IdMeta, float.Parse(boxmeta.Text));
                    AdvertenciasAsignacion.Text = "Meta asignada";
                }
                else {
                    AdvertenciasAsignacion.Text = "La meta de esta categoria para este empleado ya fue asignada";
                }
            }
            else {
                AdvertenciasAsignacion.Text = "No se ha generado la meta del mes para este empleado, por favor generala";
            }
        }
    }
}