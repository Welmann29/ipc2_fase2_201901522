﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminRegistro.aspx.cs" Inherits="Proyeco1_IPC2_.AdminRegistro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA-->
    <div class="city">
        <h1 style="text-align: center">REGISTRAR EMPLEADOS NUEVOS</h1>
    </div>
    <!-- FIN CABECERA-->

    <section class="content">
        <div class="row">
            <!--FORMULARIO DE REGISTRO DE CLIENTES-->
            <div class="col-md-6">
                <div class="productos">
                    
                        <div class="form-group">
                            <label>NIT*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxnit" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NOMBRES*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxnombre" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>APELLIDO*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxapellido" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>FECHA NACIMIENTO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxfechainicio" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>DIRECCION*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxdireccion" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NUMERO DE CELULAR*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcelular" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                   
                </div>
            </div>
            <!--FORMULARIO DE REGISTRO DE CLIENTES-->

            <!--FORMULARIO DE REGISTRO DE CLIENTES 2-->
            <div class="col-md-6">
                <div class="productos">
                    
                        <div class="form-group">
                            <label>TIPO EMPLEADO</label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlSexo" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1">01-VENDEDOR</asp:ListItem>
                                <asp:ListItem Value="2">02-SUPERVISOR</asp:ListItem>
                                <asp:ListItem Value="3">03-GERENTE</asp:ListItem>
                                <asp:ListItem Value="4">04-ADMINISTRADOR</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>NUMERO TELEFONO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxtelefono" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>CORREO ELECTRONICO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcorreoelectronico" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>CONFIRMACION CORREO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcorreo2" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label>CONTRASEÑA</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcontraseña" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>CONFIRMACION CONTRASEÑA</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcontraseña2" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                    
                </div>
            </div>
            <!--FORMULARIO DE REGISTRO DE CLIENTES 2-->
        </div>


        <!--BOTONES-->
         <div align="center">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="btn btn-primary" Width="200px" Text="Registrar" OnClick="btnRegistrar_Click"  />
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-danger" Width="200px" Text="Cancelar" />
                    </td>
                </tr>
            </table>
        </div>
        <!--FIN DE BOTONES-->
        <div align="center">
            <label>

            <asp:Label ID="AdvertenciaRegistro" runat="server" ></asp:Label>

            </label>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
