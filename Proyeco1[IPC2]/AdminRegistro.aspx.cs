﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class AdminRegistro : System.Web.UI.Page
    {
        public Inserciones Inserciones = new Inserciones();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if ((boxcontraseña.Text.Equals(boxcontraseña2.Text)) && (boxcorreoelectronico.Text.Equals(boxcorreo2.Text))) {
                string FechaNacimiento = "";
                FechaNacimiento = boxfechainicio.Text;
                DateTime fecha = DateTime.Parse(FechaNacimiento);
                FechaNacimiento = fecha.ToString("yyyy/MM/dd");

                Inserciones.Empleado(boxnit.Text, "NULL", boxnombre.Text, boxapellido.Text, boxdireccion.Text, boxtelefono.Text, boxcelular.Text, boxcorreoelectronico.Text, Int32.Parse(ddlSexo.SelectedValue), FechaNacimiento, boxcontraseña.Text);
                AdvertenciaRegistro.Text = "Registro exitoso";
            }
            else {
                AdvertenciaRegistro.Text = "Las confirmaciones no coinciden";
            }
        }
    }
}