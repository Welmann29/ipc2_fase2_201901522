﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminRoles.aspx.cs" Inherits="Proyeco1_IPC2_.AdminRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>ADMINISTRACION ROLES</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--AQUI SE VERAN LAS OPCIONES DE ASIGNACION DE ROLES-->
    <section class="content">
        <div class="row">
            <!--ASIGNACION A GERENTES-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>GERENTES</h3>
                    </div>
                    <div class="form-group">
                        <p>Aqui puedes elegir el gerente de cada supervisor</p>
                    </div>
                    <div class="form-group">
                        <label>GERENTE ENCARGADO</label>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="listgerentes" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>SUPERVISOR A ASIGNAR</label>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="Supervisores" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <p>
                         <asp:Button ID="Asignar" runat="server" CssClass="btn btn-flat" OnClick="Asignar_Click" Text="Asignar" />
                     </p>  
                           
                </div>
            </div>
            <!--ASIGNACION A GERENTES FIN-->

            <!--ASIGNACION A SUPERVISORES-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>SUPERVISORES</h3>
                    </div>
                    <div class="form-group">
                        <p>Aqui puedes elegir el supervisor de cada vendedor</p>
                    </div>
                    <div class="form-group">
                        <label>SUPERVISOR ENCARGADO</label>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="supervisores2" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>VENDEDOR A ASIGNAR</label>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="vendedores" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                      <p>
                          <asp:Button ID="AsignarVendedor" runat="server" CssClass="btn btn-flat" OnClick="AsignarVendedor_Click" Text="Asignar" />
                      </p>     
                </div>
            </div>
            <!--ASIGNACION A SUPERVISORES FIN-->
        </div>
    </section>
    <!--AQUI SE VERAN LAS OPCIONES DE ASIGNACION DE ROLES-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
