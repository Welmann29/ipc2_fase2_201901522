﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class AdminRoles : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public Inserciones Inserciones = new Inserciones();
        protected void Page_Load(object sender, EventArgs e)
        {
            string vendedorescon = "SELECT Nombres + ' ' + Apellidos as NombreCompleto, NITEmpleado FROM EMPLEADO WHERE TipoEmpleado = 1";
            string supervisorescon = "SELECT Nombres + ' ' + Apellidos as NombreCompleto, NITEmpleado FROM EMPLEADO WHERE TipoEmpleado = 2";
            string gerentes = "SELECT Nombres + ' ' + Apellidos as NombreCompleto, NITEmpleado FROM EMPLEADO WHERE TipoEmpleado = 3";

            if (!IsPostBack)
            {
                vendedores.DataSource = Dset(vendedorescon);
                vendedores.DataMember = "datos";
                vendedores.DataTextField = "NombreCompleto";
                vendedores.DataValueField = "NITEmpleado";
                vendedores.DataBind();

                Supervisores.DataSource = Dset(supervisorescon);
                Supervisores.DataMember = "datos";
                Supervisores.DataTextField = "NombreCompleto";
                Supervisores.DataValueField = "NITEmpleado";
                Supervisores.DataBind();

                supervisores2.DataSource = Dset(supervisorescon);
                supervisores2.DataMember = "datos";
                supervisores2.DataTextField = "NombreCompleto";
                supervisores2.DataValueField = "NITEmpleado";
                supervisores2.DataBind();

                listgerentes.DataSource = Dset(gerentes);
                listgerentes.DataMember = "datos";
                listgerentes.DataTextField = "NombreCompleto";
                listgerentes.DataValueField = "NITEmpleado";
                listgerentes.DataBind();

            }

        }
        protected DataSet Dset(string sentencia)
        {

            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected void AsignarVendedor_Click(object sender, EventArgs e)
        {
            Inserciones.AsignacionSupervisor(supervisores2.SelectedValue, vendedores.SelectedValue);
        }

        protected void Asignar_Click(object sender, EventArgs e)
        {
            Inserciones.AsignacionSupervisor(listgerentes.SelectedValue, Supervisores.SelectedValue);
        }
    }
}