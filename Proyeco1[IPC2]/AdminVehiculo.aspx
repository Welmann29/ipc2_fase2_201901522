﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminVehiculo.aspx.cs" Inherits="Proyeco1_IPC2_.AdminVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: darkslateblue;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>ADMINISTRACION VEHICULOS</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--AQUI SE VERAN LAS OPCIONES DE ADMINISTRACION-->
    <section class="content">
        <div class="row">
            <!--REGISTRO DE VEHICULOS-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                            <label>PLACA</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxplaca" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NUMERO DE MOTOR</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxmotor" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NUMERO DE CHASIS</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxchasis" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>ESTILO</label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="listestilo" runat="server" CssClass="form-control">
                                <asp:ListItem>Sedan</asp:ListItem>
                                <asp:ListItem>HatchBack</asp:ListItem>
                                <asp:ListItem>Camioneta</asp:ListItem>
                                <asp:ListItem>Pick-up</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>MODELO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxmodelo" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>MARCA</label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="listmarca" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1">Toyota</asp:ListItem>
                                <asp:ListItem Value="2">Nissan</asp:ListItem>
                                <asp:ListItem Value="3">KIA</asp:ListItem>
                                <asp:ListItem Value="4">Mazda</asp:ListItem>
                                <asp:ListItem Value="5">Hyundai</asp:ListItem>
                                <asp:ListItem Value="6">Honda</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    <div align="center">
                        <p>

                            <asp:Button ID="RegistroVehiculo" runat="server" Text="Registrar Vehiculo" CssClass="btn btn-danger" OnClick="RegistroVehiculo_Click" />
                        </p>
                    </div>
                    <div align="center">
                        <label>

                        <asp:Label ID="AdvertenciasRegistro" runat="server" ></asp:Label>

                        </label>
                    </div>
                </div>
            </div>
            <!--REGISTRO DE VEHICULOS FIN-->


            <!--ASIGNACION DE VEHICULOS-->
            <div class="col-md-4">
                <div class="form-group">
                    <h3>ASIGNACION VEHICULO</h3>
                </div>
                <div class="form-group">
                    <p>Si el usuario posee un vehiculo, o el vehiculo ya esta asignado proceda a desasignar antes</p>
                </div>
                <div class="form-group">
                    <label>NIT EMPLEADO A ASIGNAR</label>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="listempleadosasignar" runat="server" CssClass="form-control" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>PLACA VEHICULO A ASIGNAR</label>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="listvehiculoasignar" runat="server" CssClass="form-control" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
                <div align="center">
                    
                    <asp:Button ID="AsignarVehiculo" runat="server" CssClass="btn btn-danger" Text="Asignar Vehiculo" OnClick="AsignarVehiculo_Click" />
                    
                </div>
                <div align="Center">
                    <label>

                    <asp:Label ID="AdvertenciasAsignacion" runat="server" ></asp:Label>

                    </label>
                </div>
            </div>
            <!--ASIGNACION DE VEHICULOS FIN-->


            <!--DESASIGNACION DE VEHICULOS-->
            <div class="col-md-4">
                <div class="form-group">
                    <h3>DESASIGNACION VEHICULO</h3>
                </div>
                
                <div class="form-group">
                    <label>NIT EMPLEADO A DESASIGNAR</label>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="listempleadodesasignar" runat="server" CssClass="form-control">
                                <asp:ListItem>5448466-k</asp:ListItem>
                                <asp:ListItem>02-Empresa</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>PLACA VEHICULO A DESASIGNAR</label>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="listvehiculodesasignar" runat="server" CssClass="form-control">
                                <asp:ListItem>CP-458967</asp:ListItem>
                                <asp:ListItem>02-Empresa</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <p>
                    <a class="btn btn-default" runat="server" href="~/RegistroClientesV.aspx">Desasignar Vehiculo &raquo;</a>
                </p>
            </div>
            <!--DESASIGNACION DE VEHICULOS FIN -->


        </div>
    </section>
    <!--AQUI SE VERAN LAS OPCIONES DE ADMINISTRACION-->


    <!--CABECERA DISPONIBILIDAD-->
    <div class="city">
        <h1>INVENTARIO VEHICULOS</h1>
    </div>
    <!--FIN CABECERA DISPONIBILADAD-->

    <!--AQUI SE VERA EL INVENTARIO DE VEHICULOS-->
    <section class="content">
        <div class="row">
            <!-- VEHICULOS-->
            <div class="col-md-3">
                <div class="productos">
                    <div class="form-group">
                           <label>PLACA</label>
                    </div>
                    <div class="form-group">
                           <p>cx-548313</p>
                    </div>
                    <div class="form-group">
                           <label>Asociado a:</label>
                    </div>
                    <div class="form-group">
                           <p>Juan Perez</p>
                    </div>
                </div>
            </div>
            <!-- VEHICULOS FIN-->

        </div>
    </section>
    <!--AQUI SE VERA EL INVENTARIO DE VEHICULOS-->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
