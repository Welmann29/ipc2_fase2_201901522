﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class AdminVehiculo : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public Inserciones Inserciones = new Inserciones();
        protected void Page_Load(object sender, EventArgs e)
        {
            string gerentes = "SELECT Nombres + ' ' + Apellidos as NombreCompleto, NITEmpleado FROM EMPLEADO WHERE TipoEmpleado = 3 OR TipoEmpleado = 2";
            string vehiculos = "SELECT Placa FROM VEHICULOEMPRESA";

            if (!IsPostBack) {
                listempleadosasignar.DataSource = Dset(gerentes);
                listempleadosasignar.DataMember = "datos";
                listempleadosasignar.DataTextField = "NombreCompleto";
                listempleadosasignar.DataValueField = "NITEmpleado";
                listempleadosasignar.DataBind();

                listvehiculoasignar.DataSource = Dset(vehiculos);
                listvehiculoasignar.DataMember = "datos";
                listvehiculoasignar.DataTextField = "Placa";
                listvehiculoasignar.DataValueField = "Placa";
                listvehiculoasignar.DataBind();
            }

        }

        protected DataSet Dset(string sentencia)
        {

            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected void RegistroVehiculo_Click(object sender, EventArgs e)
        {
            Inserciones.RegistroVehiculos(boxplaca.Text, boxmotor.Text, boxchasis.Text, boxmodelo.Text, listestilo.SelectedValue, Int32.Parse(listmarca.SelectedValue));
            AdvertenciasRegistro.Text = "Registro exitoso";
        }

        protected void AsignarVehiculo_Click(object sender, EventArgs e)
        {
            Inserciones.AsignacionDeVehiculo(listvehiculoasignar.SelectedValue, listempleadosasignar.SelectedValue);
            AdvertenciasAsignacion.Text = "Asignacion exitosa";
        }
    }
}