﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vendedor.Master" AutoEventWireup="true" CodeBehind="CarritoV.aspx.cs" Inherits="Proyeco1_IPC2_.CarritoV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: tomato;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
</style>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>CARRITO COMPRAS</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

   
    <div class="row">
        <!--CREACION O MODIFICACION DE ORDENES-->
        <div class="col-md-4">
            <div class="cliente">
                <!--SECCION ORDEN NUEVA-->
                <div class="form-group">
                    <h3>EMPEZAR ORDEN NUEVA</h3>
                </div>
                <div class="form-group">
                    <label>NIT CLIENTE</label>
                </div>
                <div class="form-group">
                    <p>
                        Ingresa el NIT del cliente que desea esta orden, el cliente debe estar registrado, si no es un cliente registrado, 
                        <a  runat ="server" href="~/RegistroClientesV.aspx"> registralo aqui.</a> 
                    </p>
                </div>
                <div class="form-group">
                        <asp:TextBox ID="boxNIT" runat="server" Text="" CssClass="form-control" Width="500px"></asp:TextBox>
                </div>
                <p>
                    <asp:Button ID="BotonOrdenNueva" runat="server" Text="Generar Orden" CssClass="btn-vk" OnClick="BotonOrdenNueva_Click" />
                    <asp:Label ID="AvisosNuevaOrden" runat="server" ></asp:Label>
                </p>
                <!--SECCION ORDEN NUEVA FIN-->



                <!--SECCION ORDEN EXISTENTE-->
                <div class="form-group">
                    <h3>MODIFICAR ORDEN EXISTENTE</h3>
                </div>
                <div class="form-group">
                    <label>NUMERO DE ORDEN</label>
                </div>
                <div class="form-group">
                    <p>
                        Ingresa el numero de orden que deseas modificar, recuerda, para modificar la orden esta no debe estar cerrada, si la orden se cerro,
                        ya no puede ser modificada, si no recuerdas el numero de orden, 
                        <a  runat ="server" href="~/ControlOrdenesV.aspx"> verifica tus ordenes aqui.</a> 
                    </p>
                </div>
                <div class="form-group">
                        <asp:TextBox ID="boxOrden" runat="server" Text="" CssClass="form-control" Width="500px"></asp:TextBox>
                </div>
                <p>
                    <asp:Button ID="Button1" runat="server" Text="Modificar Orden" CssClass="btn-vk" OnClick="Button1_Click" />
                    <asp:Label ID="AvisosModificarOrden" runat="server" ></asp:Label>
                </p>
                <!--SECCION ORDEN EXISTENTE FIN-->
            </div>
        </div>
        <!--CREACION O MODIFICACION DE ORDENES FIN-->


        <!--INFORMACION DE ORDEN ACTUAL-->
        <div class="col-md-4">
            <div class="productos">
                
                <div class="form-group">
                    <H3>ORDEN ACTUAL</H3>
                </div>
                <div class="form-group">
                    <LABEL>NUMERO DE ORDEN</LABEL>
                </div>
                <div class="form-group">
                    <p>
                        <asp:Label ID="NumeroOrdenActual" runat="server"></asp:Label>
                    </p>
                </div>
                <div class="form-group">
                    <LABEL>NIT CLIENTE</LABEL>
                </div>
                <div class="form-group">
                    <p>
                        <asp:Label ID="NITOrdenActual" runat="server"></asp:Label>
                    </p>
                </div>
                <div class="form-group">
                    <LABEL>SALDO DISPONIBLE</LABEL>
                </div>
                <div class="form-group">
                    <p>
                       $ <asp:Label ID="SaldoDisponible" runat="server"></asp:Label>
                    </p>
                </div>
                <div class="form-group">
                    <LABEL>TOTAL DE LA ORDEN</LABEL>
                </div>
                <div class="form-group">
                    <p align="right">
                        $<asp:Label ID="TotalOrdenActual" runat="server"></asp:Label>
                    </p>
                </div>
                <p>
                    <!--CONEXION A BASE DE DATOS-->
                    <asp:Button ID="CerarrOrden" runat="server" CssClass="btn-instagram" Text="Cerrar Orden" OnClick="CerrarOrden_Click" />
                    <asp:Label ID="AvisoCierre" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <!--INFORMACION DE ORDEN ACTUAL FIN-->


        <!--Productos seleccionados-->
        <div class="col-md-4">
            <div class="productos">
                <div class="form-group">
                    <label>AGREGAR PRODUCTOS A LA ORDEN:</label>
                </div>
                <div class="form-group">
                    <label>Codigo del producto:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cantidad:&nbsp; </label>&nbsp;<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; <asp:TextBox ID="CodigoProducto" runat="server" Width="156px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="Cantidad" runat="server">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Button ID="AgregarProducto" runat="server" CssClass="btn-facebook" OnClick="AgregarProducto_Click" Text="Agregar " />
                    </p>
                    <p>
                        <asp:Label ID="AdvertenciaProducto" runat="server" ></asp:Label>
                    </p>
                </div>
                <div class="form-group">
                    <label>PRODUCTOS DE LA ORDEN:</label>
                </div>
                <div>
                    <p align="center">
                        <asp:GridView ID="Orden" runat="server" align="center" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Codigo" HeaderText="Codigo Producto   " />
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre del producto   " />
                                <asp:BoundField DataField="Precio" HeaderText="Precio   " />
                                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad    " />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                            <SortedAscendingHeaderStyle BackColor="#848384" />
                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                            <SortedDescendingHeaderStyle BackColor="#575357" />
                        </asp:GridView>
                    </p>
                </div>
                <div class="form-group">
                    <label>ELIMINAR PRODUCTOS DE LA ORDEN:</label>
                </div>
                <div>
                    <p>Codigo a eliminar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cantidad:</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="CodigoEliminar" runat="server"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="CantidadEliminar" runat="server">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="EliminarProducto" runat="server" Text="Eliminar" CssClass="btn-facebook" OnClick="EliminarProducto_Click" />
                        </p>
                    <p>

                        <asp:Label ID="AdvertenciaEliminar" runat="server" ></asp:Label>

                    </p>
                </div>
                
                
            </div>
        </div>
        <!--Productos seleccionados-->


    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <p>&copy; <%: DateTime.Now.Year %> - Pagina vendedor</p>
</asp:Content>
