﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class CarritoV : System.Web.UI.Page
    {
        //Variable que me indicara en que orden se trabaja actualmente 
        public static int NoOrden = 0;
        //Variable que me indicara el total de la orden actual
        public static float TotalPagar = 0;
        //Variable que nos indicara cuanto puede gastar el cliente 
        public static float Disponible = 0;
        //Variable del cliente actuañ
        public static string NITactual = "";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BotonOrdenNueva_Click(object sender, EventArgs e)
        {
            AvisoCierre.Text = "";
            string NITCliente = "";
            string NITComprobacion = "";
            NITCliente = boxNIT.Text;
            NITactual = NITCliente;

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITCliente);
            SqlCommand comando = new SqlCommand("SELECT NITCliente, LimiteCredito, Gastado FROM CLIENTE WHERE NITCliente = @nit" , sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                NITComprobacion = lector.GetString(0);
                Disponible = (lector.GetFloat(1) - lector.GetFloat(2));
            }
            sqlCon.Close();

            if (NITCliente.Equals(NITComprobacion)) {
                AvisosNuevaOrden.Text = "";
                using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
                {
                    sqlCon1.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO ORDEN VALUES(0, 0, '" + NITCliente + "', '" + WebForm1.NITusuario + "', 1, 4, '" + DateTime.Now.ToString("yyyy/MM/dd") + "', NULL, NULL, NULL)", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);

                    SqlCommand comando1 = new SqlCommand("SELECT TOP(1) IdOrden FROM ORDEN ORDER BY IdOrden DESC", sqlCon1);
                    SqlDataReader lector1 = comando1.ExecuteReader();
                    while (lector1.Read())
                    {
                        NoOrden = lector1.GetInt32(0);
                    }
                    sqlCon1.Close();

                    NumeroOrdenActual.Text = NoOrden.ToString();
                    NITOrdenActual.Text = NITCliente;
                    TotalPagar = 0;
                    TotalOrdenActual.Text = TotalPagar.ToString();
                    PorGastar(0);
                    SaldoDisponible.Text = Disponible.ToString();
                    ActualizarOrden();
                    
                }
            }
            else {
                AvisosNuevaOrden.Text = "Cliente no registrado";
            }
        }

        protected void AgregarProducto_Click(object sender, EventArgs e)
        {
            float monto = 0;
            int idcarrito = 0;
            if (NoOrden == 0) {
                AdvertenciaProducto.Text = "No estas trabajando en una orden";
            }
            else { 
            int CodigoProduct = 0;
            int CodigoComprobacion = 0;

            CodigoProduct = Int32.Parse(CodigoProducto.Text);

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();

            

            SqlParameter parnomus = new SqlParameter("@code", CodigoProduct);
            
            SqlCommand comando = new SqlCommand("SELECT CodigoProducto FROM PRODUCTO WHERE CodigoProducto = @code", sqlCon);
            comando.Parameters.Add(parnomus);
            

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                CodigoComprobacion = lector.GetInt32(0);
            }

            sqlCon.Close();

            if (CodigoProduct == CodigoComprobacion) {
                AdvertenciaProducto.Text = "";


                    if (!EstaEnLAOrden(CodigoProduct))
                    {
                        //INSERCION DE UN PRODUCTO NUEVO EN LA ORDEN
                        using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
                        {
                            sqlCon1.Open();
                            SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO CARRITO VALUES(" + NoOrden + ", " + CodigoProduct + ", " + Cantidad.SelectedValue + ", " + WebForm1.litaActual + ")", sqlCon1);
                            DataTable dtbl = new DataTable();
                            sqlDa.Fill(dtbl);
                            sqlCon1.Close();
                        }

                        SqlConnection sqlCon2 = new SqlConnection(connectionString);
                        sqlCon2.Open();

                        SqlCommand comando2 = new SqlCommand("SELECT TOP(1) INVENTARIO.Precio, CARRITO.Cantidad, CARRITO.IdCarrito FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " ORDER BY IdCarrito DESC", sqlCon2);

                        SqlDataReader lector2 = comando2.ExecuteReader();
                        while (lector2.Read())
                        {

                            monto = (lector2.GetFloat(0) * lector2.GetInt32(1));
                            idcarrito = lector2.GetInt32(2);
                        }

                        sqlCon2.Close();


                        if (PuedeSeguir(monto))
                        {

                            TotalPagar = TotalPagar + monto;
                            TotalOrdenActual.Text = TotalPagar.ToString();
                            PorGastar(monto);
                            SaldoDisponible.Text = Disponible.ToString();

                            using (SqlConnection sqlCon11 = new SqlConnection(connectionString))
                            {
                                sqlCon11.Open();
                                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN set  TotalAPagar = " + TotalPagar + " WHERE IdOrden = " + NoOrden, sqlCon11);
                                DataTable dtbl = new DataTable();
                                sqlDa.Fill(dtbl);
                                sqlCon11.Close();
                            }

                            using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                            {
                                sqlCon3.Open();
                                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + NoOrden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", sqlCon3);
                                DataTable dtbl1 = new DataTable();
                                sqlDa.Fill(dtbl1);
                                Orden.DataSource = dtbl1;
                                Orden.DataBind();
                                sqlCon3.Close();
                            }
                        }
                        else
                        {
                            AdvertenciaProducto.Text = "El cliente no tiene credito suficiente";
                            using (SqlConnection sqlCon12 = new SqlConnection(connectionString))
                            {
                                sqlCon12.Open();
                                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CARRITO SET IdOrden = 1 WHERE IdCarrito = " + idcarrito, sqlCon12);
                                DataTable dtbl = new DataTable();
                                sqlDa.Fill(dtbl);
                                sqlCon12.Close();
                            }
                            ActualizarOrden();
                        }
                        //FIN INCERSION DE PODUCTO NUEVO EN LA ORDEN
                    }
                    else {
                        //MODIFICACION CANTIDAD DE PRODUCTOS
                        int EnLaOrden = CuantosHay(CodigoProduct);
                        float precio = CuantoVale(CodigoProduct, WebForm1.litaActual);
                        int CantidadAgregar = Int32.Parse(Cantidad.SelectedValue);
                        if (PuedeSeguir(CantidadAgregar * precio))
                        {
                            AgregarCarrito(CodigoProduct, CantidadAgregar);
                            ModificarParametrosAgregar(precio, CantidadAgregar);
                            ActualizarOrden();
                        }
                        else {
                            AdvertenciaProducto.Text = "El cliente no tiene credito suficiente";
                        }


                    }

                }
                else {
                AdvertenciaProducto.Text = "Codigo invalido, intentelo de nuevo";
            }
            }
        }

        protected void CerrarOrden_Click(object sender, EventArgs e)
        {
            if (TotalPagar == 0)
            {
                AvisoCierre.Text = "No se han agregado productos";
            }
            else
            {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";

                using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
                {
                    sqlCon1.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN set EstadoAprobacion = 5, TotalAPagar = " + TotalPagar + ", FechaCerrada = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE IdOrden = " + NoOrden, sqlCon1);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    sqlCon1.Close();
                }
                AvisoCierre.Text = "Orden enviada para aprobacion";
                NoOrden = 0;
                TotalPagar = 0;
                NumeroOrdenActual.Text = "";
                NITOrdenActual.Text = "";
                TotalOrdenActual.Text = "";
                
                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int orden = Int32.Parse(boxOrden.Text);
            int comprobacionOrden = 0;
            float total = 0;
            string clienteOrden = "";
            string comprobacionEmpleado = "";
            int estadoOrden = 0;

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);
            SqlCommand comando = new SqlCommand("SELECT * FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                comprobacionOrden = lector.GetInt32(0);
                total = lector.GetFloat(1);
                clienteOrden = lector.GetString(3);
                comprobacionEmpleado = lector.GetString(4);
                estadoOrden = lector.GetInt32(6);
            }
            sqlCon.Close();

            if (orden == comprobacionOrden)
            {
                if (estadoOrden == 4)
                {
                    if (comprobacionEmpleado.Equals(WebForm1.NITusuario))
                    {
                        TotalPagar = total;
                        NoOrden = orden;
                        using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                        {
                            sqlCon3.Open();
                            SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + NoOrden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", sqlCon3);
                            DataTable dtbl1 = new DataTable();
                            sqlDa.Fill(dtbl1);
                            Orden.DataSource = dtbl1;
                            Orden.DataBind();
                            sqlCon3.Close();
                        }

                        NumeroOrdenActual.Text = NoOrden.ToString();
                        NITOrdenActual.Text = clienteOrden;
                        TotalOrdenActual.Text = TotalPagar.ToString();
                        NITactual = clienteOrden;
                        PorGastar(0);
                        SaldoDisponible.Text = Disponible.ToString();
                        AvisoCierre.Text = "";
                        SaldoDisponible.Text = "";
                    }
                    else {
                        AvisosModificarOrden.Text = "Esta orden fue generada por otro usuario";
                    }
                }
                else {
                    AvisosModificarOrden.Text = "La orden ya fue cerrada";
                }

            }
            else {
                AvisosModificarOrden.Text = "No se encontro ninguna orden con este numero";
            }

        }

        protected void ActualizarOrden() {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
            {
                sqlCon3.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + NoOrden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", sqlCon3);
                DataTable dtbl1 = new DataTable();
                sqlDa.Fill(dtbl1);
                Orden.DataSource = dtbl1;
                Orden.DataBind();
                sqlCon3.Close();
            }
        }

        protected void PorGastar(float saldo) {

            float limite = 0;
            float gastado = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITactual);
            SqlCommand comando = new SqlCommand("SELECT LimiteCredito, Gastado FROM CLIENTE WHERE NITCliente = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                limite = lector.GetFloat(0);
                gastado = lector.GetFloat(1);
            }
            sqlCon.Close();
            Disponible = limite - gastado - saldo;

            //Aqui se le ingresara el nuevo valor al valor gastado 
            gastado = gastado + saldo;

            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CLIENTE SET Gastado = " + gastado + " WHERE NITCliente = '" + NITactual + "'", sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        protected Boolean PuedeSeguir(float saldo) {
            float limite = 0;
            float gastado = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITactual);
            SqlCommand comando = new SqlCommand("SELECT LimiteCredito, Gastado FROM CLIENTE WHERE NITCliente = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                limite = lector.GetFloat(0);
                gastado = lector.GetFloat(1);
            }
            sqlCon.Close();
            float dispo = limite - gastado - saldo;

            if (dispo < 0)
            {
                return false;
            }
            else {
                return true;
            }
        }

        //Metodos utilizados para eliminar el producto de la orden
        protected Boolean EstaEnLAOrden(int codigo) {
            int codigoComprobacion = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@codigo", codigo);
            SqlParameter parnomu = new SqlParameter("@orden", NoOrden);
            SqlCommand comando = new SqlCommand("SELECT CodigoProducto FROM CARRITO WHERE IdOrden = @orden AND CodigoProducto = @codigo", sqlCon);
            comando.Parameters.Add(parnomus);
            comando.Parameters.Add(parnomu);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                codigoComprobacion = lector.GetInt32(0);
                
            }
            sqlCon.Close();

            if (codigo == codigoComprobacion) {
                return true;
            }
            else {
                return false;
            }
        }

        protected int CuantosHay(int codigo) {
            int cantidad = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@codigo", codigo);
            SqlParameter parnomu = new SqlParameter("@orden", NoOrden);
            SqlCommand comando = new SqlCommand("SELECT Cantidad FROM CARRITO WHERE IdOrden = @orden AND CodigoProducto = @codigo", sqlCon);
            comando.Parameters.Add(parnomus);
            comando.Parameters.Add(parnomu);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                cantidad = lector.GetInt32(0);
            }
            sqlCon.Close();

            return cantidad;
        }

        protected float CuantoVale(int codigo, int listaActual) {
            float precio = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@codigo", codigo);
            SqlParameter parnomu = new SqlParameter("@lista", listaActual);
            SqlCommand comando = new SqlCommand("  SELECT Precio FROM INVENTARIO WHERE CodigoLista = @lista AND CodigoProducto = @codigo", sqlCon);
            comando.Parameters.Add(parnomus);
            comando.Parameters.Add(parnomu);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                precio = lector.GetFloat(0);
            }
            sqlCon.Close();

            return precio;
        }

        protected void EliminarDeOrden(int codigo, int cantidad) {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            int cantidadSet = CuantosHay(codigo) - cantidad;
            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CARRITO SET Cantidad = " + cantidadSet + " WHERE IdOrden = " + NoOrden + " AND CodigoProducto = " + codigo, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        protected void ModificarParametros(float precio, int cantidad) {
            TotalPagar = TotalPagar - (precio * cantidad);
            float limiteCredito = 0;
            
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                
                using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
                {
                    sqlCon1.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN SET TotalAPagar = " + TotalPagar + " WHERE IdOrden = " + NoOrden, sqlCon1);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    sqlCon1.Close();
                }

            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            sqlCon3.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITactual);
            SqlCommand comando = new SqlCommand("SELECT  LimiteCredito FROM CLIENTE WHERE NITCliente = @nit", sqlCon3);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                limiteCredito = lector.GetFloat(0);
                
            }
            sqlCon3.Close();



            Disponible = Disponible + (precio * cantidad);

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CLIENTE SET Gastado = " + (limiteCredito - Disponible) + " WHERE NITCliente = '" + NITactual + "'", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }

            SaldoDisponible.Text = Disponible.ToString();
            TotalOrdenActual.Text = TotalPagar.ToString();
        }

        protected void QuitarDeOrden(int codigo) {
            int idcarrito = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@codigo", codigo);
            SqlParameter parnomu = new SqlParameter("@orden", NoOrden);
            SqlCommand comando = new SqlCommand("SELECT IdCarrito FROM CARRITO WHERE IdOrden = @orden AND CodigoProducto = @codigo", sqlCon);
            comando.Parameters.Add(parnomus);
            comando.Parameters.Add(parnomu);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                idcarrito = lector.GetInt32(0);
            }
            sqlCon.Close();

            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CARRITO SET IdOrden = 1 WHERE IdCarrito = " + idcarrito, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }

        }

        protected void EliminarProducto_Click(object sender, EventArgs e)
        {
            int codigo = Int32.Parse(CodigoEliminar.Text);
            int cantidad = Int32.Parse(CantidadEliminar.SelectedValue);
            int Cuantos = CuantosHay(codigo);
            float precio = CuantoVale(codigo, WebForm1.litaActual);

            if (EstaEnLAOrden(codigo)) {
                if (cantidad >= Cuantos) {
                    QuitarDeOrden(codigo);
                    ModificarParametros(precio, Cuantos);
                    ActualizarOrden();
                }
                else {
                    EliminarDeOrden(codigo, cantidad);
                    ModificarParametros(precio, cantidad);
                    ActualizarOrden();
                }
            }
            else {
                AdvertenciaEliminar.Text = "El producto que intenta eliminar no esta en la orden";
            }
        }

        //Utilizado para modificar la cantidad de productos
        protected void ModificarParametrosAgregar(float precio, int cantidad) {
            TotalPagar = TotalPagar + (precio * cantidad);
            float limiteCredito = 0;

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";

            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN SET TotalAPagar = " + TotalPagar + " WHERE IdOrden = " + NoOrden, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }

            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            sqlCon3.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITactual);
            SqlCommand comando = new SqlCommand("SELECT  LimiteCredito FROM CLIENTE WHERE NITCliente = @nit", sqlCon3);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                limiteCredito = lector.GetFloat(0);

            }
            sqlCon3.Close();



            Disponible = Disponible - (precio * cantidad);

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CLIENTE SET Gastado = " + (limiteCredito - Disponible) + " WHERE NITCliente = '" + NITactual + "'", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }

            SaldoDisponible.Text = Disponible.ToString();
            TotalOrdenActual.Text = TotalPagar.ToString();
        }

        protected void AgregarCarrito(int codigo, int cantidad) {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            int cantidadSet = CuantosHay(codigo) + cantidad;
            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CARRITO SET Cantidad = " + cantidadSet + " WHERE IdOrden = " + NoOrden + " AND CodigoProducto = " + codigo, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }
    }
}