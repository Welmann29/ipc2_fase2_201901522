﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Proyeco1_IPC2_.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>CONTACTA AL ADMINISTRADOR DE LA PAGINA</h2>
    <h3>Si tienes algun problema con la pagina, contacta al administrador</h3>
    <address>
        Facultad de Ingenieria<br />
        Ciudad Universitaria, Zona 12, Guatemala<br />
        <abbr title="Celular">P:</abbr>
        41335696
    </address>

    <address>
        <strong>Correo Personal:</strong>   <a href="mailto:welmann2903@gmail.com">welmann2903@gmail.com</a><br />
        <strong>Correo Institucional:</strong> <a href="mailto:2070654460116@ingenieria.usac.edu.gt">2070654460116@ingenieria.usac.edu.gt</a>
    </address>
</asp:Content>
