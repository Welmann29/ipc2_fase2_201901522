﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Gerente.Master" AutoEventWireup="true" CodeBehind="ControlOrdenesG.aspx.cs" Inherits="Proyeco1_IPC2_.ControlOrdenesG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: black;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
   </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>MIS ORDENES</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--CUERPO LISTA DE ORDENES APROBADAS-->
    <section class="content">
        <div class="row">
           <div class="col-md-4">
               <div class="productos">
                   <div>
                       <label>SELECCIONA LAS ORDENES QUE DESEAS VER</label>
                   </div>
                   <div>
                       <p>En este apartado podras seleccionar las ordenes que has creado bajo tu usuario segun su estado actual, podras ver,
                           ordenes aun en modificacion, ordenes cerradas (Pendientes de aprobacion), ordenes aprobadas y ordenes canceladas en su totalidad.
                       </p>
                   </div>
                   <div>

                       <asp:DropDownList ID="OrdenesSeleccionadas" runat="server" CssClass="alert-dismissable" Height="16px" Width="225px">
                           <asp:ListItem Value="4">En Modificacion</asp:ListItem>
                           <asp:ListItem Value="5">Pendientes de aprobacion</asp:ListItem>
                           <asp:ListItem Value="6">Aprobada </asp:ListItem>
                           <asp:ListItem Value="3">Cancelada</asp:ListItem>
                       </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:Button ID="VerOrden" runat="server" CssClass="btn-danger" Text="Ver Ordenes" OnClick="VerOrden_Click" />

                   </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="productos">
                    <asp:GridView ID="Orden" runat="server" align="center" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanged="Orden_SelectedIndexChanged">
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <Columns>
                                <asp:BoundField DataField="IdOrden" HeaderText="Codigo Orden   " />
                                <asp:BoundField DataField="ClienteOrden" HeaderText="NIT del cliente   " />
                                <asp:BoundField DataField="TotalAPagar" HeaderText="Total a pagar   " />
                                <asp:BoundField DataField="CantidadPagada" HeaderText="Cantidad pagada   " />
                            </Columns>
                            
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                            
                        </asp:GridView>
                    
               </div>
           </div>
            <div class="col-md-4">
                <div class="productos">
                    <div>
                        <label>ANULAR O APROBAR ORDENES</label>
                    </div>
                    <div>
                        <p>Aqui podras anular ordenes que ya no desees realizar, para poder eliminar la orden esta no debe tener abonos realizados, al ser gerente tambien puedes aprobar tus propias ordenes:</p>
                    </div>
                    <div>
                        <label>Ingrese el No. de orden:</label>
                    </div>
                    <div>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <asp:TextBox ID="TextBox1" runat="server" Width="295px" align="center"></asp:TextBox>

                    </div>
                    <div>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Anulador" runat="server" CssClass="btn-bitbucket" Text="Anular Orden" OnClick="Anulador_Click" />

                    &nbsp;
                        <asp:Button ID="Button3" runat="server" CssClass="btn-danger" OnClick="Button3_Click" Text="Aprobar Orden" />

                    </div>
                    <div>
                        <p>
                            <asp:Label ID="AdvertenciaAnulador" runat="server" ></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CUERPO LISTA DE ORDENES APROBADAS FIN-->

    <!--CABECERA ORDENES SUBALTERNOS-->
    <div class="city">
        <h1>ORDENES SUBALTERNOS</h1>
    </div>
    <!--FIN CABECERA ORDENS SUBALTERNOS-->

    <!--CUERPO DE ORDENES SUBALTERNOS-->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="productos">
                    <div>
                        <label>INGRESA EL SUBALTERNO QUE DESEAS CONSULTAR</label>
                    </div>
                    <div>
                        <p>Ingresa el NIT del subalterno que deseas verificar, seguido de ello elige el tipo de orden que deseas ver, justo como lo hacias 
                            en tus propias ordenes, si deseas ver el listado de tus subalternos, selecciona el boton, "Ver subalternos"
                        </p>
                    </div>
                    <div>
                        <label>NIT del subalterno a verificar:</label>
                    </div>
                    <div>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="NITSubalterno" runat="server" Width="313px"></asp:TextBox>

                    </div>
                    <div>
                        <p>
                            <asp:Label ID="AdvertenciasOrdenesSA" runat="server" ></asp:Label>.
                        </p>
                    </div>
                    <div>

                       <asp:DropDownList ID="OrdenesSeleccionadasSA" runat="server" CssClass="alert-dismissable" Height="16px" Width="225px">
                           <asp:ListItem Value="4">En Modificacion</asp:ListItem>
                           <asp:ListItem Value="5">Pendientes de aprobacion</asp:ListItem>
                           <asp:ListItem Value="6">Aprobada </asp:ListItem>
                           <asp:ListItem Value="3">Cancelada</asp:ListItem>
                       </asp:DropDownList>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="VerificarOrdenesSA" runat="server" CssClass="btn-danger" Text="Verificar" OnClick="VerificarOrdenesSA_Click" />

                    </div>
                    <div>

                        <asp:Label ID="AvisosOrdenesSubAlterno" runat="server" ></asp:Label>

                    </div>
                    <div>
                        <p>¿No recuerdas el NIT de alguno de tus SubAlternos? Haz click en el boton para ver el listado de tus subalternos con su 
                            nombre y NIT
                        </p>
                    </div>
                    <div>

                        <asp:Button ID="ListadoSubAlternos" runat="server" Text="Ver listado" align="center" CssClass="btn-dropbox" OnClick="ListadoSubAlternos_Click"/>

                    </div>
                    <div>

                        <asp:GridView ID="SubAlternos" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                            
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="productos">
                    <asp:GridView ID="OrdenSA" runat="server" align="center" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" >
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <Columns>
                                <asp:BoundField DataField="IdOrden" HeaderText="Codigo Orden   " />
                                <asp:BoundField DataField="ClienteOrden" HeaderText="NIT del cliente   " />
                                <asp:BoundField DataField="TotalAPagar" HeaderText="Total a pagar   " />
                                <asp:BoundField DataField="CantidadPagada" HeaderText="Cantidad pagada   " />
                            </Columns>
                            
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                            
                        </asp:GridView>
                </div>
            </div>
            <div class="col-md-4">
                <div class="productos">
                    <div>
                        <div>
                            <label>APROBAR O CERRAR ORDENES</label>
                        </div>
                        <div>
                            <p>En este apartado puedes aprobar o cerrar ordenes segun su estado actual, ingresa la orden que deseas modificar
                                y presiona el boton que necesites, para poder aprobar una orden debe estar cerrada o "Pendiente de aprobacion",
                                si deseas cerrar una orden esta debe estar "En modificacion"
                            </p>
                        </div>
                        <div>

                            <asp:TextBox ID="OrdenAprobaroCerrar" runat="server" CssClass="btn-block"></asp:TextBox>

                        </div>
                        <div align="Center">

                        
                            <asp:Button ID="Aprobador" runat="server" CssClass="btn-bitbucket" Text="Aprobar " OnClick="Aprobador_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button2" runat="server" CssClass="btn-danger" Text="Cerrar" OnClick="Button2_Click" />

                        </div>
                        <div align="Center">

                            <asp:Button ID="AnularSubordinado" runat="server" CssClass="btn btn-success" Text="Anular Orden" OnClick="AnularSubordinado_Click" />

                        </div>
                        <div>
                            <p>

                                <asp:Label ID="AdvertenciasAprobarCerrar" runat="server" ></asp:Label>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FIN CUERPO DE ORDENES SUBALTERNOS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
