﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class ControlOrdenesG : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public string NITEmpresa = "220000-0";
        public string TelefonoEmpresa = "24425698";
        public string DireccionEmpresa = "4ta. Av. A, 5-34, Zona 3, Ciudad de Guatemala";
        public string NombreEmpresa = "Distribuidora De Productos Magnificos DIPROMA S.A.";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Orden_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void MostrarOrdenes(int Estado)
        {
            if (Estado == 3)
            {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoOrden = " + Estado + " AND EmpleadoOrden = '" + WebForm1.NITusuario + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    Orden.DataSource = dtbl1;
                    Orden.DataBind();
                    sqlCon3.Close();
                }
            }
            else
            {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoAprobacion = " + Estado + " AND EmpleadoOrden = '" + WebForm1.NITusuario + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    Orden.DataSource = dtbl1;
                    Orden.DataBind();
                    sqlCon3.Close();
                }
            }
        }

        protected void VerOrden_Click(object sender, EventArgs e)
        {
            int estado = Int32.Parse(OrdenesSeleccionadas.SelectedValue);
            MostrarOrdenes(estado);
        }

        protected Boolean PuedeSerAnulada(string orden)
        {
            int idOrden = 0;
            string empleadoOrden = "";
            int EstadoOrden = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@IdOrden", orden);

            SqlCommand comando = new SqlCommand(" SELECT IdOrden, EmpleadoOrden, EstadoOrden FROM ORDEN WHERE IdOrden = @IdOrden", sqlCon);
            comando.Parameters.Add(parnomus);


            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                idOrden = lector.GetInt32(0);
                empleadoOrden = lector.GetString(1);
                EstadoOrden = lector.GetInt32(2);
            }
            sqlCon.Close();

            if ((idOrden == Int32.Parse(orden)) && (empleadoOrden.Equals(WebForm1.NITusuario)) && (EstadoOrden == 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void AnularOrden(string orden)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";

            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN SET EstadoOrden = 4 WHERE IdOrden = " + orden, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        protected void Anulador_Click(object sender, EventArgs e)
        {
            if (PuedeSerAnulada(TextBox1.Text))
            {
                AnularOrden(TextBox1.Text);
                ModificarEstadisticas(TextBox1.Text);
                AdvertenciaAnulador.Text = "Orden anulada. Actualice lista de ordenes";
            }
            else if (SePuedeAnularYaPagada(Int32.Parse(TextBox1.Text)))
            {
                AnularOrden(TextBox1.Text);
                ModificarEstadisticas(TextBox1.Text);
                RealizarFacturaPorAnulacion(TextBox1.Text);
                GeneracionPDFparaFactura(TextBox1.Text);
                AdvertenciasAprobarCerrar.Text = "Orden anulada";
            }
            else {
                AdvertenciaAnulador.Text = "No se puede anular esta orden";
            }
        }

        protected void ModificarEstadisticas(string orden)
        {
            float gastadoCliente = 0;
            float GastoOrden = 0;
            string NitCliente = "";

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);

            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);
            SqlCommand comando = new SqlCommand("SELECT  ClienteOrden, TotalAPAgar FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                NitCliente = lector.GetString(0);
                GastoOrden = lector.GetFloat(1);
            }
            sqlCon.Close();

            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            sqlCon3.Open();
            SqlParameter parno = new SqlParameter("@nit", NitCliente);
            SqlCommand comando1 = new SqlCommand("SELECT  Gastado FROM CLIENTE WHERE NITCliente = @nit", sqlCon3);
            comando1.Parameters.Add(parno);

            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                gastadoCliente = lector1.GetFloat(0);

            }
            sqlCon3.Close();

            gastadoCliente = gastadoCliente - GastoOrden;

            using (SqlConnection sqlCon11 = new SqlConnection(connectionString))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CLIENTE SET Gastado = " + gastadoCliente + " WHERE NITCliente = '" + NitCliente + "'", sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }
        }


        //Metodos para controlar las ordenes de los sub Alternos

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void MostrarEmpleados()
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
            {
                sqlCon3.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT NITEmpleado, Nombres, Apellidos FROM EMPLEADO where NITSupervisor = '" + WebForm1.NITusuario + "'", sqlCon3);
                DataTable dtbl1 = new DataTable();
                sqlDa.Fill(dtbl1);
                SubAlternos.DataSource = dtbl1;
                SubAlternos.DataBind();
                sqlCon3.Close();
            }
        }

        protected void ListadoSubAlternos_Click(object sender, EventArgs e)
        {
            MostrarEmpleados();
        }

        protected Boolean EsMiSubalterno(string nit)
        {
            string nitComprobacion = "";
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", nit);

            SqlCommand comando = new SqlCommand("SELECT NITSupervisor FROM EMPLEADO WHERE NITEmpleado = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    nitComprobacion = lector.GetString(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }

            if (WebForm1.NITusuario.Equals(nitComprobacion))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void MostrarOrdenSubAlterno(int estado, string nit)
        {
            if (estado == 3)
            {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoOrden = " + estado + " AND EmpleadoOrden = '" + nit + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    OrdenSA.DataSource = dtbl1;
                    OrdenSA.DataBind();
                    sqlCon3.Close();
                }
            }
            else
            {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoAprobacion = " + estado + " AND EmpleadoOrden = '" + nit + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    OrdenSA.DataSource = dtbl1;
                    OrdenSA.DataBind();
                    sqlCon3.Close();
                }
            }
        }

        protected void VerificarOrdenesSA_Click(object sender, EventArgs e)
        {
            AdvertenciasOrdenesSA.Text = "";
            string NitVerificar = NITSubalterno.Text;
            int estado = Int32.Parse(OrdenesSeleccionadasSA.SelectedValue);
            if (EsMiSubalterno(NitVerificar))
            {
                MostrarOrdenSubAlterno(estado, NitVerificar);
            }
            else
            {
                AdvertenciasOrdenesSA.Text = "El empleado no es tu subalterno, no puedes ver sus ordenes";
            }
        }

        //Fin metodos para controlar ordenes de SubAlternos


        //Metodos para Aprobar o Cerrar una orden
        protected Boolean LaOrdenEsDeMiSubalterno(int orden)
        {
            string nitEmpleadoOrden = "";


            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);

            SqlCommand comando = new SqlCommand("SELECT EmpleadoOrden FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    nitEmpleadoOrden = lector.GetString(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //Pondremos un aviso en el puto label mierda que muestra las advertencias
            }

            if (EsMiSubalterno(nitEmpleadoOrden))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Para anular la orden ya pagada de un subalterno
        protected Boolean SePuedeAnularYaPagada(int orden)
        {

            int EstadoOrden = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@IdOrden", orden);

            SqlCommand comando = new SqlCommand(" SELECT EstadoOrden FROM ORDEN WHERE IdOrden = @IdOrden", sqlCon);
            comando.Parameters.Add(parnomus);


            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {

                EstadoOrden = lector.GetInt32(0);
            }
            sqlCon.Close();

            if (EstadoOrden == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void RealizarFacturaPorAnulacion(string orden)
        {
            string update = "UPDATE ORDEN SET  EstadoOrden = 4, FechaCancelada = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE IdOrden = " + orden;
            string Factura = "INSERT INTO FACTURA VALUES(1010, '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + orden + ")";

            //Se llevaran a cabo las sentencias correspondientes
            using (SqlConnection sqlCon11 = new SqlConnection(connectionStrin))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(Factura, sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }

            using (SqlConnection sqlCon1 = new SqlConnection(connectionStrin))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(update, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        public DataTable dtProductos(string orden)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad, CARRITO.Cantidad * INVENTARIO.Precio AS PrecioTotal FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + orden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", conn);

                sqlDa.Fill(dt);


            }
            return dt;
        }

        protected void GeneracionPDFparaFactura(string orden)
        {
            string nombreCliente = "";
            string direccionCliente = "";
            string clienteOrden = "";
            string TelefonoCliente = "";
            float totalOrden = 0;
            float cantidadPagada = 0;
            string empleadoNombre = "";
            int NoFactura = 0;

            //Obtencion de datos de la orden
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT ClienteOrden, TotalAPagar, CantidadPagada FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    clienteOrden = lector.GetString(0);
                    totalOrden = lector.GetFloat(1);
                    cantidadPagada = lector.GetFloat(2);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion de datos de la orden


            //Obtencion datos cliente
            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();
            SqlParameter parnomu = new SqlParameter("@cliente", clienteOrden);

            SqlCommand comando1 = new SqlCommand("SELECT Nombres, Direccion, Celular FROM CLIENTE WHERE NITCliente =  @cliente", sqlCon1);
            comando1.Parameters.Add(parnomu);

            try
            {
                SqlDataReader lector1 = comando1.ExecuteReader();
                while (lector1.Read())
                {
                    nombreCliente = lector1.GetString(0);
                    direccionCliente = lector1.GetString(1);
                    TelefonoCliente = lector1.GetString(2);
                }
                sqlCon1.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos cliente

            //Obtencion datos aprobador
            SqlConnection sqlCon21 = new SqlConnection(connectionString);
            sqlCon21.Open();
            SqlParameter parno = new SqlParameter("@empleado", WebForm1.NITusuario);

            SqlCommand comando21 = new SqlCommand("SELECT Nombres, Apellidos FROM EMPLEADO WHERE NITEmpleado = @empleado", sqlCon21);
            comando21.Parameters.Add(parno);

            try
            {
                SqlDataReader lector21 = comando21.ExecuteReader();
                while (lector21.Read())
                {
                    empleadoNombre = lector21.GetString(0) + " " + lector21.GetString(1);

                }
                sqlCon21.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos aprobador



            //Obtencion del numero de factura
            SqlConnection sqlCon211 = new SqlConnection(connectionString);
            sqlCon211.Open();
            SqlParameter parn = new SqlParameter("@orden", orden);

            SqlCommand comando211 = new SqlCommand("  SELECT NoFactura FROM FACTURA WHERE OrdenGenerada = @orden", sqlCon211);
            comando211.Parameters.Add(parn);

            try
            {
                SqlDataReader lector211 = comando211.ExecuteReader();
                while (lector211.Read())
                {
                    NoFactura = lector211.GetInt32(0);

                }
                sqlCon211.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion del numero de factura


            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Factura NO. " + NoFactura + " de la Orden No." + orden, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("Datos de la empresa:" + "\n"));
            p.Add(new Chunk("NIT: " + NITEmpresa + "\n"));
            p.Add(new Chunk("Nombre: " + NombreEmpresa + "\n"));
            p.Add(new Chunk("Direccion: " + DireccionEmpresa + "\n"));
            p.Add(new Chunk("Telefono: " + TelefonoEmpresa + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Datos de cliente:" + "\n"));
            p.Add(new Chunk("NIT: " + clienteOrden + "\n"));
            p.Add(new Chunk("Nombre: " + nombreCliente + "\n"));
            p.Add(new Chunk("Direccion: " + direccionCliente + "\n"));
            p.Add(new Chunk("Telefono: " + TelefonoCliente + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Monto de la Factura: $" + cantidadPagada + "\n"));
            p.Add(new Chunk("Monto de la Orden: $" + totalOrden + "\n"));
            p.Add(new Chunk("Fecha de emision de la Factura: " + DateTime.Now.ToString("dd/MM/yyyy") + "\n"));
            p.Add(new Chunk("Orden anulada por: " + WebForm1.NITusuario + " Nombre: " + empleadoNombre + "\n"));
            p.Add(new Chunk("Detalle de articulos: " + "\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(orden);
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);


            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Paragraph(20, "NOTA DE CREDITO, Orden No." + orden, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p1 = new Phrase();

            p1.Add(new Chunk("Fecha de anulacion: " + DateTime.Now.ToString("dd/MM/yyyy") + "\n"));
            p1.Add(new Chunk("Codigo orden anulada: " + orden + "\n"));
            p1.Add(new Chunk("Monto de la nota de credito: $" + (totalOrden - cantidadPagada) + "\n"));



            document.Add(p1);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=NotaDeCreditoNo." + NoFactura + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();
        }

        //Utilizaremos Modificar Estadisticas Para restarle al clieten su deuda
        //Para anular la orden ya pagada de un subalterno

        protected Boolean SePuedeCerrar(int orden)
        {
            int estado = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT EstadoAprobacion FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    estado = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }

            if (estado == 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean SePuedeAprobar(int orden)
        {
            int estado = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT EstadoAprobacion FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    estado = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }

            if (estado == 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void AprobarOCerarOrden(int orden, int modificador)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            string modificarFecha = "";
            if (modificador == 5)
            {
                modificarFecha = ", FechaCerrada = '";
            }
            else
            {
                modificarFecha = ", FechaAprobacion = '";
            }
            using (SqlConnection sqlCon11 = new SqlConnection(connectionString))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN SET EstadoAprobacion = " + modificador + modificarFecha + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE IdOrden = " + orden, sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }
        }

        protected void Aprobador_Click(object sender, EventArgs e)
        {
            int orden = Int32.Parse(OrdenAprobaroCerrar.Text);
            if (LaOrdenEsDeMiSubalterno(orden))
            {
                if (SePuedeAprobar(orden))
                {
                    AprobarOCerarOrden(orden, 6);
                    AdvertenciasAprobarCerrar.Text = "La orden se ha aprobado";
                    GenerarPDF(orden, 1);
                }
                else
                {
                    AdvertenciasAprobarCerrar.Text = "No fue posible aprobar la orden";
                }

            }
            else
            {
                AdvertenciasAprobarCerrar.Text = "La orden no es de uno de tus SubAlternos";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int orden = Int32.Parse(OrdenAprobaroCerrar.Text);
            if (LaOrdenEsDeMiSubalterno(orden))
            {
                if (SePuedeCerrar(orden))
                {
                    AprobarOCerarOrden(orden, 5);
                    AdvertenciasAprobarCerrar.Text = "La orden se ha cerrado";
                }
                else
                {
                    AdvertenciasAprobarCerrar.Text = "No fue posible cerrar la orden";
                }

            }
            else
            {
                AdvertenciasAprobarCerrar.Text = "La orden no es de uno de tus SubAlternos";
            }
        }

        public DataTable dtProductos(int orden)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad, CARRITO.Cantidad * INVENTARIO.Precio AS PrecioTotal FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + orden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", conn);

                sqlDa.Fill(dt);


            }
            return dt;
        }

        protected void GenerarPDF(int orden, int Aprobado)
        {
            float totalPagar = 0;
            string clienteOrden = "";
            string empleadoOrden = "";
            DateTime fechaCreacion = DateTime.Now;
            DateTime fechaCerrada = DateTime.Now;
            DateTime fechaAprobada = DateTime.Now;
            string nombreCliente = "";
            string direccionCliente = "";
            string nombreVendedor = "";
            string nombreAprobador = "";
            string puestoVendedor = "";
            int idPuesto = 0;


            //Obtencion de datos de la orden
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT * FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    totalPagar = lector.GetFloat(1);
                    clienteOrden = lector.GetString(3);
                    empleadoOrden = lector.GetString(4);
                    fechaCreacion = lector.GetDateTime(7);
                    fechaCreacion = lector.GetDateTime(8);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion de datos de la orden

            //Obtencion datos cliente
            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();
            SqlParameter parnomu = new SqlParameter("@cliente", clienteOrden);

            SqlCommand comando1 = new SqlCommand("SELECT Nombres, Direccion FROM CLIENTE WHERE NITCliente =  @cliente", sqlCon1);
            comando1.Parameters.Add(parnomu);

            try
            {
                SqlDataReader lector1 = comando1.ExecuteReader();
                while (lector1.Read())
                {
                    nombreCliente = lector1.GetString(0);
                    direccionCliente = lector1.GetString(1);
                }
                sqlCon1.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos cliente



            //Obtencion datos vendedor
            SqlConnection sqlCon2 = new SqlConnection(connectionString);
            sqlCon2.Open();
            SqlParameter parnom = new SqlParameter("@empleado", empleadoOrden);

            SqlCommand comando2 = new SqlCommand("SELECT Nombres FROM EMPLEADO WHERE NITEmpleado = @empleado", sqlCon2);
            comando2.Parameters.Add(parnom);

            try
            {
                SqlDataReader lector2 = comando2.ExecuteReader();
                while (lector2.Read())
                {
                    nombreVendedor = lector2.GetString(0);

                }
                sqlCon2.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos cliente

            //Obtencion datos aprobador
            SqlConnection sqlCon21 = new SqlConnection(connectionString);
            sqlCon21.Open();
            SqlParameter parno = new SqlParameter("@empleado", WebForm1.NITusuario);

            SqlCommand comando21 = new SqlCommand("SELECT Nombres, Apellidos FROM EMPLEADO WHERE NITEmpleado = @empleado", sqlCon21);
            comando21.Parameters.Add(parno);

            try
            {
                SqlDataReader lector21 = comando21.ExecuteReader();
                while (lector21.Read())
                {
                    nombreAprobador = lector21.GetString(0) + " " + lector21.GetString(1);

                }
                sqlCon21.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos aprobador


            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Orden Aprobada No." + orden, fontTitle));
            document.Add(new Chunk("\n"));

            string puesto = "";
            if (Aprobado == 1) {
                puesto = "Supervisor";
            }
            else {
                puesto = "Gerente";
            }

            Phrase p = new Phrase();
            p.Add(new Chunk("Cliente: " + clienteOrden + ", " + nombreCliente + ", direccion: " + direccionCliente + "\n"));
            p.Add(new Chunk("Vendedor: " + empleadoOrden + ", " + nombreVendedor + ", Puesto: " + puesto + "\n"));
            p.Add(new Chunk("Fecha de creacion de la orden: " + fechaCreacion.ToString("dd/MM/yyyy") + "\n"));
            TimeSpan difFechas = fechaCerrada - fechaCreacion;
            p.Add(new Chunk("Fecha de orden cerrada: " + fechaCerrada.ToString("dd/MM/yyyy") + " Dias para cerrar: " + difFechas.Days + "\n"));
            TimeSpan difFechas1 = fechaCerrada - fechaAprobada;
            p.Add(new Chunk("Fecha aprobacion de orden: " + fechaAprobada.ToString("dd/MM/yyyy") + " Dias para aprobar: " + difFechas.Days) + "\n");
            p.Add(new Chunk("Aprobado por: " + nombreAprobador + " Puesto: Gerente" + "\n"));
            p.Add(new Chunk("Lista: " + WebForm1.litaActual + "\n"));
            p.Add(new Chunk("Valor total: $" + totalPagar + "\n"));
            p.Add(new Chunk("Detalles de la orden: " + "\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(orden);
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);



            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=AprobacionDeOrden" + orden + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string orden = TextBox1.Text;
            int ordenInt = Int32.Parse(orden);
            if (PuedeSerAnulada(orden)) {
                if (SePuedeAprobar(ordenInt)) {
                    AprobarOCerarOrden(ordenInt, 6);
                    GenerarPDF(ordenInt, 2);
                }
                else {
                    AdvertenciaAnulador.Text = "Esta orden no puede ser aprobada";
                }
            }
            else {
                AdvertenciaAnulador.Text = "Esta orden no es tuya";
            }
        }

        protected void AnularSubordinado_Click(object sender, EventArgs e)
        {
            string OrdenString = OrdenAprobaroCerrar.Text;
            int ordenInt = Int32.Parse(OrdenString);
            if (LaOrdenEsDeMiSubalterno(ordenInt))
            {
                if (SePuedeAnularYaPagada(ordenInt))
                {
                    AnularOrden(OrdenString);
                    ModificarEstadisticas(OrdenString);
                    RealizarFacturaPorAnulacion(OrdenString);
                    GeneracionPDFparaFactura(OrdenString);
                    AdvertenciasAprobarCerrar.Text = "Orden anulada";
                }
                else
                {
                    AdvertenciasAprobarCerrar.Text = "Esta orden no puede ser anulada";
                }

            }
            else
            {
                AdvertenciasAprobarCerrar.Text = "Esta orden no es de tus subalternos";
            }
        }
        //Metodos para Aprobar o Cerrar una orden
    }
}