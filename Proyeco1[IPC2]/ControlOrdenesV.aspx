﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vendedor.Master" AutoEventWireup="true" CodeBehind="ControlOrdenesV.aspx.cs" Inherits="Proyeco1_IPC2_.ControlOrdenesV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: tomato;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>CONTROL DE ORDENES</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--CUERPO LISTA DE ORDENES APROBADAS-->
    <section class="content">
        <div class="row">
           <div class="col-md-4">
               <div class="productos">
                   <div>
                       <label>SELECCIONA LAS ORDENES QUE DESEAS VER</label>
                   </div>
                   <div>
                       <p>En este apartado podras seleccionar las ordenes que has creado bajo tu usuario segun su estado actual, podras ver,
                           ordenes aun en modificacion, ordenes cerradas (Pendientes de aprobacion), ordenes aprobadas y ordenes canceladas en su totalidad.
                       </p>
                   </div>
                   <div>

                       <asp:DropDownList ID="OrdenesSeleccionadas" runat="server" CssClass="alert-dismissable" Height="16px" Width="225px">
                           <asp:ListItem Value="4">En Modificacion</asp:ListItem>
                           <asp:ListItem Value="5">Pendientes de aprobacion</asp:ListItem>
                           <asp:ListItem Value="6">Aprobada </asp:ListItem>
                           <asp:ListItem Value="3">Cancelada</asp:ListItem>
                       </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <asp:Button ID="VerOrden" runat="server" CssClass="btn-danger" Text="Ver Ordenes" OnClick="VerOrden_Click" />

                   </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="productos">
                    <asp:GridView ID="Orden" runat="server" align="center" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanged="Orden_SelectedIndexChanged">
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <Columns>
                                <asp:BoundField DataField="IdOrden" HeaderText="Codigo Orden   " />
                                <asp:BoundField DataField="ClienteOrden" HeaderText="NIT del cliente   " />
                                <asp:BoundField DataField="TotalAPagar" HeaderText="Total a pagar   " />
                                <asp:BoundField DataField="CantidadPagada" HeaderText="Cantidad pagada   " />
                            </Columns>
                            
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                            
                        </asp:GridView>
                    
               </div>
           </div>
            <div class="col-md-4">
                <div class="productos">
                    <div>
                        <label>ANULAR ORDENES</label>
                    </div>
                    <div>
                        <p>Aqui podras anular ordenes que ya no desees realizar, para poder eliminar la orden esta no debe tener abonos realizados:</p>
                    </div>
                    <div>
                        <label>Ingrese el No. de orden por anular:</label>
                    </div>
                    <div>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <asp:TextBox ID="TextBox1" runat="server" Width="295px" align="center"></asp:TextBox>

                    </div>
                    <div>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Anulador" runat="server" CssClass="btn-bitbucket" Text="Anular Orden" OnClick="Anulador_Click" />

                    </div>
                    <div>
                        <p>
                            <asp:Label ID="AdvertenciaAnulador" runat="server" ></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CUERPO LISTA DE ORDENES APROBADAS FIN-->

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
