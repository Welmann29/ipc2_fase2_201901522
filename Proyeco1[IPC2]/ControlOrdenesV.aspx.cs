﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class ControlOrdenesV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Orden_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void MostrarOrdenes(int Estado) {
            if (Estado == 3) {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoOrden = " + Estado + " AND EmpleadoOrden = '" + WebForm1.NITusuario + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    Orden.DataSource = dtbl1;
                    Orden.DataBind();
                    sqlCon3.Close();
                }
            }
            else {
                string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
                using (SqlConnection sqlCon3 = new SqlConnection(connectionString))
                {
                    sqlCon3.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT IdOrden, ClienteOrden, TotalAPAgar, CantidadPagada FROM ORDEN WHERE EstadoAprobacion = " + Estado + " AND EmpleadoOrden = '" + WebForm1.NITusuario + "' AND EstadoOrden != 4 ORDER BY IdOrden DESC", sqlCon3);
                    DataTable dtbl1 = new DataTable();
                    sqlDa.Fill(dtbl1);
                    Orden.DataSource = dtbl1;
                    Orden.DataBind();
                    sqlCon3.Close();
                }
            }
        }

        protected void VerOrden_Click(object sender, EventArgs e)
        {
            int estado = Int32.Parse(OrdenesSeleccionadas.SelectedValue);
            MostrarOrdenes(estado);
        }

        protected Boolean PuedeSerAnulada(string orden) {
            int idOrden = 0;
            string empleadoOrden = "";
            int EstadoOrden = 0;
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@IdOrden", orden);
            
            SqlCommand comando = new SqlCommand(" SELECT IdOrden, EmpleadoOrden, EstadoOrden FROM ORDEN WHERE IdOrden = @IdOrden", sqlCon);
            comando.Parameters.Add(parnomus);
            

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                idOrden = lector.GetInt32(0);
                empleadoOrden = lector.GetString(1);
                EstadoOrden = lector.GetInt32(2);
            }
            sqlCon.Close();

            if ((idOrden == Int32.Parse(orden)) && (empleadoOrden.Equals(WebForm1.NITusuario)) && (EstadoOrden == 1)) {
                return true;
            }
            else {
                return false;
            }
        }

        protected void AnularOrden(string orden) {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";

            using (SqlConnection sqlCon1 = new SqlConnection(connectionString))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE ORDEN SET EstadoOrden = 4 WHERE IdOrden = " + orden, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        protected void Anulador_Click(object sender, EventArgs e)
        {
            if (PuedeSerAnulada(TextBox1.Text)) {
                AnularOrden(TextBox1.Text);
                ModificarEstadisticas(TextBox1.Text);
                AdvertenciaAnulador.Text = "Orden anulada. Actualice lista de ordenes";
            }
            else {
                AdvertenciaAnulador.Text = "No es posible anular esta orden.";
            }
        }

        protected void ModificarEstadisticas(string orden) {
            float gastadoCliente = 0;
            float GastoOrden = 0;
            string NitCliente = "";

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);

            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);
            SqlCommand comando = new SqlCommand("SELECT  ClienteOrden, TotalAPAgar FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                NitCliente = lector.GetString(0);
                GastoOrden = lector.GetFloat(1);
            }
            sqlCon.Close();

            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            sqlCon3.Open();
            SqlParameter parno = new SqlParameter("@nit", NitCliente);
            SqlCommand comando1 = new SqlCommand("SELECT  Gastado FROM CLIENTE WHERE NITCliente = @nit", sqlCon3);
            comando1.Parameters.Add(parno);

            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                gastadoCliente = lector1.GetFloat(0);

            }
            sqlCon3.Close();

            gastadoCliente = gastadoCliente - GastoOrden;

            using (SqlConnection sqlCon11 = new SqlConnection(connectionString))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("UPDATE CLIENTE SET Gastado = " + gastadoCliente + " WHERE NITCliente = '" + NitCliente + "'", sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }
        }

    }
}