﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Gerente.Master" AutoEventWireup="true" CodeBehind="ControlSupervisoresG.aspx.cs" Inherits="Proyeco1_IPC2_.ControlSupervisoresG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: black;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--Cabecera-->   
        <div class="city">
                    <h1 alig="center">Supervisores Asignados</h1>      
        </div> 
    <!--FIN CABECERA-->

    <!--REGISTRO DE LOS SUPERVISORES ASIGNADOS-->
    <section class="content">
        <div class="row">
            <!--PARA CADA SUPERVISOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesSupervisoresG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA SUPERVISOR FIN-->

            <!--PARA CADA SUPERVISOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesSupervisoresG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA SUPERVISOR FIN-->

            <!--PARA CADA SUPERVISOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesSupervisoresG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA SUPERVISOR FIN-->
        </div>
    </section>
    <!--FIN REGISTRO DE LOS SUPERVISORES ASIGNADOS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
