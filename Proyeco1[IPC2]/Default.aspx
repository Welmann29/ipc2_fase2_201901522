﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Proyeco1_IPC2_._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>DIPROMA</h1>
        <p class="lead">Distribuidora de productos magnificos, S.A.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Inicia Sesion &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Nuevo en la empresa</h2>
            <p>
                Registra todos tus datos para tener un usuario dentro de la plataforma de la empresa, recuerda que necesitas autorizacion de 
                un superior a cargo.
            </p>
            <p>
                <a class="btn btn-default" runat ="server" href="~/About">Registro &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Contacto al administrador</h2>
            <p>
                Tienes algun problema con tu usuario, contacta con el supervisor a cargo, aqui podras encontrar su informacion.
            </p>
            <p>
                <a class="btn btn-default" runat ="server" href="~/Contact">Contacto &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Un pequeño recorrido</h2>
            <p>
                No te ha quedado del todo claro como usar la plataforma, ve este tutorial para empezar a trabajar
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301950">Ver tutorial &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
