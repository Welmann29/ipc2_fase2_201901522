﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Gerente.Master" AutoEventWireup="true" CodeBehind="DetallesSupervisoresG.aspx.cs" Inherits="Proyeco1_IPC2_.DetallesSupervisoresG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: teal;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>CONTROL DE ORDENES #nombre supervisor#</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--CUERPO LISTA DE ORDENES-->
    <section class="content">
        <div class="row">
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
   
        </div>
    </section>
    <!--CUERPO LISTA DE ORDENES FIN-->


    <!--CABECERA FACTURAS-->
    <div class="city">
        <h1>FACTURAS #nombre supervisor#</h1>
    </div>
    <!--FIN CABECERA FACTURAS-->

    <!--CUERPO FACTURAS-->
    <section class="content">
        <div class="row">
            <!--CADA FACTURA-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. FACTURA:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>NO SERIE:</label>
                    </div>
                    <div class="form-group">
                        <P>48966552545</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>FECHA DE EMISION:</label>
                    </div>
                    <div class="form-group">
                        <P>29/03/2020</P>
                    </div>
                </div>
            </div>
            <!--FIN FACTURA-->
            
   
        </div>
    </section>
    <!--CUERPO FACTURAS FIN-->

    <!--CABECERA VENDEDORES DE SUPERVISOR-->
    <div class="city">
        <h1>VENDEDORES DE #nombre supervisor#</h1>
    </div>
    <!--FIN CABECERA VENDEDORES DE SUPERVISOR-->

    <!--REGISTRO DE LOS VENDEDORES ASIGNADOS-->
    <section class="content">
        <div class="row">
            <!--PARA CADA VENDEDOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA VENDEDOR FIN-->

            <!--PARA CADA VENDEDOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA VENDEDOR FIN-->

            <!--PARA CADA VENDEDOR-->
             <div class="col-md-3">
                <div class="cliente">
                    <div class="form-group">
                        <h4 alig="center">Juan Gabriel Perez Montiel</h4>
                    </div>
                    <div class="form-group">
                        <label>NIT:</label>
                    </div>
                    <div class="form-group">
                        <p>4589645-k</p>
                    </div>
                    <div class="form-group">
                        <label>META DE VENTAS:</label>
                    </div>
                    <div class="form-group">
                        <p>$1500</p>
                    </div>
                    <div class="form-group">
                        <label>MONTO VENDIDO:</label>
                    </div>
                    <div class="form-group">
                        <p>$1200</p>
                    </div>
                    <p>
                        <a class="btndetalles" runat="server" href="~/DetallesVendedorG">Ver detalles &raquo;</a>
                    </p>
                </div>
             </div>
            <!--PARA CADA VENDEDOR FIN-->
        </div>
    </section>
    <!--FIN REGISTRO DE LOS VENDEDORES ASIGNADOS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
