﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supervisor.Master" AutoEventWireup="true" CodeBehind="DetallesVendedorS.aspx.cs" Inherits="Proyeco1_IPC2_.DetallesVendedorS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: tomato;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>CONTROL DE ORDENES #nombre vendedor#</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--CUERPO LISTA DE ORDENES-->
    <section class="content">
        <div class="row">
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
            <!--CADA ORDEN-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>SALDO PENDIENTE DE LA ORDEN:</label>
                    </div>
                    <div class="form-group">
                        <P>$0</P>
                    </div>
                </div>
            </div>
            <!--FIN ORDEN-->
   
        </div>
    </section>
    <!--CUERPO LISTA DE ORDENES FIN-->


    <!--CABECERA FACTURAS-->
    <div class="city">
        <h1>FACTURAS #nombre vendedor#</h1>
    </div>
    <!--FIN CABECERA FACTURAS-->

    <!--CUERPO FACTURAS-->
    <section class="content">
        <div class="row">
            <!--CADA FACTURA-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NO. FACTURA:</label>
                    </div>
                    <div class="form-group">
                        <P>###456489###</P>
                    </div>
                    <div class="form-group">
                        <label>NIT CLIENTE:</label>
                    </div>
                    <div class="form-group">
                        <P>###45648545459###</P>
                    </div>
                    <div class="form-group">
                        <label>NO SERIE:</label>
                    </div>
                    <div class="form-group">
                        <P>48966552545</P>
                    </div>
                    <div class="form-group">
                        <label>MONTO CANCELADO:</label>
                    </div>
                    <div class="form-group">
                        <P>$500</P>
                    </div>
                    <div class="form-group">
                        <label>FECHA DE EMISION:</label>
                    </div>
                    <div class="form-group">
                        <P>29/03/2020</P>
                    </div>
                </div>
            </div>
            <!--FIN FACTURA-->
            
   
        </div>
    </section>
    <!--CUERPO FACTURAS FIN-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
