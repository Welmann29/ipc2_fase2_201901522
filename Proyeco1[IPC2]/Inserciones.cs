﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Proyeco1_IPC2_
{
    public class Inserciones
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";

        public void Insercion(string sentencia) {
            using (SqlConnection sqlCon11 = new SqlConnection(connectionStrin))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }
        }

        public void TipoEmpleado(int codigo, string nombre) {
            string sentenciaPuesto = "INSERT INTO TIPOEMPLEADO VALUES(" + codigo + ", '" + nombre + "')";
            Insercion(sentenciaPuesto);
        }
        public void Empleado(string nit, string NITSuperior, string nombres, string apellidos, string direccion, string telefono, string celular, string email, int puesto, string FechaNacimiento, string contraseña) {
            string sentenciaEmpleado = "";
            if (NITSuperior.Equals("NULL")) {
                sentenciaEmpleado = "INSERT INTO EMPLEADO VALUES('" + nit + "', NULL, '" + nombres + "', '" + apellidos + "', '" + direccion + "', '" + celular + "', '" + telefono + "', '" + email + "', '" + FechaNacimiento + "', " + puesto + ", NULL, '" + contraseña + "')";
            }
        
            else { 
                sentenciaEmpleado = "INSERT INTO EMPLEADO VALUES('" + nit + "', '" + NITSuperior + "', '" + nombres + "', '" + apellidos + "', '" + direccion + "', '" + celular + "', '" + telefono + "', '" + email + "', '" + FechaNacimiento + "', " + puesto + ", NULL, '" + contraseña + "')";
             }
            Insercion(sentenciaEmpleado);
        }
        public void Cliente(string nit, string nombres, string apellidos, string FechaNacimiento, string direccion, string telefono, string celular, string email, int departamento, int municipio, float limiteCredito, int diasCredito, int TipoCliente) {
            string sentenciaCliente = "  INSERT INTO CLIENTE VALUES('" + nit + "', '" + nombres + "', '" + apellidos + "', '" + FechaNacimiento + "', '" + direccion + "', '" + telefono + "', '" + celular + "', '" + email + "', " + departamento + ", " + municipio + ", " + limiteCredito + ", " + diasCredito + ", " + TipoCliente+ ", 0)";
            Insercion(sentenciaCliente);
        }

        public void CategoriaProducto(int idCategoria, string nombre) {
            string sentenciaCategoria = "INSERT INTO CATEGORIA VALUES(" + idCategoria + ", '" + nombre + "')";
            Insercion(sentenciaCategoria);
        }

        public void Producto(int codigo, string nombre, string descripcion, int idCategoria) {
            string sentenciaProducto = "INSERT INTO PRODUCTO VALUES(" + codigo + ", '" + nombre + "', '" + descripcion + "', " + idCategoria + ")";
            Insercion(sentenciaProducto);
        }

        public void Lista(int codigo, string nombre, string fechaInicio, string fechaFinal) {
            string sentenciaLista = "INSERT INTO LISTAPRECIO VALUES(" + codigo + ", '" + fechaInicio + "', '" + fechaFinal + "', '" + nombre + "')";
            Insercion(sentenciaLista);
        }

        public void AsignacionPrecio(int codigoLista, int codigoProducto, float precio) {
            string sentenciaAsignacionPrecio = "INSERT INTO INVENTARIO VALUES(" + codigoLista + ", " + codigoProducto + ", " + precio + ")";
            Insercion(sentenciaAsignacionPrecio);
        }

        public void Departamento(int codigo, string nombre) {
            string sentenciaDepartamento = "INSERT INTO DEPARTAMENTO VALUES(" + codigo + ", '" + nombre + "')";
            Insercion(sentenciaDepartamento);
        }

        public void Municipio(int codigo, string nombre, int codigoDepartamento) {
            string sentenciaMunicipio = "INSERT INTO MUNICIPIO VALUES(" + codigo + ", '" + nombre + "', " + codigoDepartamento + ")";
            Insercion(sentenciaMunicipio);
        }

        public void GeneracionMeta(string nit, string fecha) {
            string sentenciaMeta = "INSERT INTO METAMES VALUES('" + fecha + "', NULL, '" + nit + "')";
            Insercion(sentenciaMeta);
        }

        public void MetaCategoria(int categoria, int idMetaMes, float valorMeta) {
            string sentenciaMetaCategoria = "INSERT INTO METACATEGORIA VALUES(" + categoria + ", " + idMetaMes + ", " + valorMeta + ")";
            Insercion(sentenciaMetaCategoria);
        }

        public void AsignacionSupervisor(string nitSupervisor, string nitEmpleado) {
            string sentenciaAsignacion = "UPDATE EMPLEADO SET NITSupervisor = '" + nitSupervisor + "' WHERE NITEmpleado = '" + nitEmpleado + "'";
            Insercion(sentenciaAsignacion);
        }

        public void RegistroVehiculos(string placa, string NumeroDeMotor, string NumeroDeChasis, string Modelo, string Estilo, int Marca) {
            string SentenciaVehiculo = "INSERT INTO VEHICULOEMPRESA VALUES('" + placa + "', '" + NumeroDeMotor + "', '" + NumeroDeChasis + "', '" + Modelo + "', '" + Estilo + "', " + Marca + ")";
            Insercion(SentenciaVehiculo);
        }

        public void AsignacionDeVehiculo(string placa, string NITEmpleado) {
            string sentenciaAsigncionVehiculo = "UPDATE EMPLEADO SET VehiculoAsignado = '" + placa + "' WHERE NITEmpleado = '" + NITEmpleado + "'";
            Insercion(sentenciaAsigncionVehiculo);
        }

        public void AsignacionListaVigente(int lista) {
            string sentenciaLIsta = "UPDATE LISTAVIGENTE SET CodigoLista = " + lista + " WHERE IdVigente = 1";
            Insercion(sentenciaLIsta);
        }

        //OBTENER DATOS PARA ANULACION  Y MODIFICACION DE LOS PARAMETROS

        public float RecuperarGastado(string NITCliente) {
            float gastado = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", NITCliente);
            SqlCommand comando = new SqlCommand("SELECT LimiteCredito, Gastado FROM CLIENTE WHERE NITCliente = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                
                gastado = lector.GetFloat(1);
            }
            sqlCon.Close();
            return gastado;
        }

        public void RestarGastado(string NITCliente, int orden) {
            float nuevoSaldo = RecuperarGastado(NITCliente) - RecuperarMontoOrden(orden);
            string sentencia = "UPDATE CLIENTE SET Gastado = " + nuevoSaldo + " WHERE NITCLiente = '" + NITCliente + "'";
            Insercion(sentencia);
        }

        public float RecuperarMontoOrden(int orden) {
            float gastado = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);
            SqlCommand comando = new SqlCommand("SELECT TotalAPagar FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                gastado = lector.GetFloat(0);
            }
            sqlCon.Close();
            return gastado;
        }

        public void Monedas(string nombre, string simbolo, float tasa) {
            string sentenciaMoneda = "INSERT INTO MONEDA VALUES('" + nombre + "', '" + simbolo + "', " + tasa + ")";
            Insercion(sentenciaMoneda);
        }

        public void ListaxCliente(string nit, int codigoLista) {
            string sentencia = " INSERT INTO LISTACLIENTE VALUES('" + nit + "', " + codigoLista + ")";
            Insercion(sentencia);
        }
    }
}