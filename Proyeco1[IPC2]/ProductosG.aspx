﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Gerente.Master" AutoEventWireup="true" CodeBehind="ProductosG.aspx.cs" Inherits="Proyeco1_IPC2_.ProductosG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: black;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA SUPERIOR-->
    <div class="city">
        <h1>PRODUCTOS</h1>
    </div>
    <!--FIN CABECERA SUPERIOR-->

    <!--BARRA OPCIONES-->
    <div class="cliente">
        <div class="form-group">
            <label>
                Categorias:
                <asp:DropDownList ID="listacategoria" runat="server" CssClass="form-control" AutoPostBack="True">
                </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
            </label>
           
        </div>
        <div>
            <asp:Button ID="VerProductos" runat="server" Text="Ver Productos" CssClass="btn btn-danger" OnClick="VerProductos_Click" />
        </div>
    </div>
    <!--BARRA OPCIONES-->


    <!--CUERPO LISTA DE PRODUCTOS-->
    <section class="content">
        <div class="row">
             <div class="col-md-12">
                <p align="Center">

                    <asp:GridView ID="Productos" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2">
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                    </asp:GridView>

                </p>
            </div>
        </div>
    </section>
    <!--FIN CUERPO LISTA DE PRODUCTOS-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
