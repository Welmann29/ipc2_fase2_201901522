﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    
    public partial class ProductosS : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        protected void Page_Load(object sender, EventArgs e)
        {
            string consulta = "SELECT * FROM CATEGORIA";

            if (!IsPostBack)
            {
                listacategoria.DataSource = Dset(consulta);
                listacategoria.DataMember = "datos";
                listacategoria.DataTextField = "NombreCategoria";
                listacategoria.DataValueField = "IdCategoria";
                listacategoria.DataBind();
            }
        }

        protected DataSet Dset(string sentencia)
        {
            
            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected void VerProductos_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlCon3 = new SqlConnection(connectionStrin))
            {
                sqlCon3.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT PRODUCTO.CodigoProducto, PRODUCTO.Nombre, INVENTARIO.Precio FROM PRODUCTO JOIN INVENTARIO ON PRODUCTO.CodigoProducto = INVENTARIO.CodigoProducto AND PRODUCTO.IdCategoria = " + listacategoria.SelectedValue + " AND INVENTARIO.CodigoLista = " + WebForm1.litaActual, sqlCon3);
                DataTable dtbl1 = new DataTable();
                sqlDa.Fill(dtbl1);
                Productos.DataSource = dtbl1;
                Productos.DataBind();
                sqlCon3.Close();
            }
        }
    }
}