﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class RegistroAbonoG : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public string NITEmpresa = "220000-0";
        public string TelefonoEmpresa = "24425698";
        public string DireccionEmpresa = "4ta. Av. A, 5-34, Zona 3, Ciudad de Guatemala";
        public string NombreEmpresa = "Distribuidora De Productos Magnificos DIPROMA S.A.";
        protected void Page_Load(object sender, EventArgs e)
        {
            string consulta = "SELECT IdOrden FROM ORDEN WHERE EmpleadoOrden = '" + WebForm1.NITusuario + "' AND EstadoAprobacion = 6 AND EstadoOrden < 3 ORDER BY IdOrden DESC";
            string consultaMoneda = "SELECT * FROM MONEDA";

            if (!IsPostBack)
            {
                NoOrden.DataSource = Dset(consulta);
                NoOrden.DataMember = "datos";
                NoOrden.DataTextField = "IdOrden";
                NoOrden.DataValueField = "IdOrden";
                NoOrden.DataBind();

                moneda.DataSource = Dset(consultaMoneda);
                moneda.DataMember = "datos";
                moneda.DataTextField = "Simbolo";
                moneda.DataValueField = "TasaCambio";
                moneda.DataBind();
            }
        }

        protected DataSet Dset(string sentencia)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        //Metodos para el abono

        //Verificaremos esto para pasar de pendiente a pagando
        protected Boolean EsSuPrimerAbono(string orden)
        {
            int EstadoOrden = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);

            SqlCommand comando = new SqlCommand("SELECT EstadoOrden FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    EstadoOrden = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //Pondremos un aviso en el puto label mierda que muestra las advertencias
            }

            if (EstadoOrden == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Se verificara que la cantidad a pagar, no exceda la deuda que se tiene
        protected Boolean SePuedePagarEsaCantidad(string orden, float MontoAPagar)
        {
            float faltante = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);

            SqlCommand comando = new SqlCommand("SELECT TotalAPagar - CantidadPagada AS Faltante FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    faltante = lector.GetFloat(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //Pondremos un aviso en el puto label mierda que muestra las advertencias
            }

            if (MontoAPagar > faltante)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Recuperaremos esto para setearle la cantidad total que llevara pagada
        protected float CantidadPagadaActual(string Orden)
        {
            float CantidadPagada = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", Orden);

            SqlCommand comando = new SqlCommand("SELECT CantidadPagada FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    CantidadPagada = lector.GetFloat(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //Pondremos un aviso en el puto label mierda que muestra las advertencias
            }
            return CantidadPagada;
        }


        //Aqui se hara el proceso de incersion en base de datos del abono y el update de la cantidad pagada de la orden
        protected void RealizarPago(string orden, int FormaPago, float MontoAPagar, float cantidadPagada)
        {
            string sentencia = "INSERT INTO ABONO VALUES(" + MontoAPagar + ", '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + FormaPago + ", " + orden + ", NULL, NULL, NULL, NULL, NULL)";
            string update = "";
            float NuevaCantidadPagada = MontoAPagar + cantidadPagada;

            //Si es su primer abono le modificaremos el estado de la orden a pagando, para que no se pueda anular
            if (EsSuPrimerAbono(orden))
            {
                update = "UPDATE ORDEN SET CantidadPagada = " + NuevaCantidadPagada + ", EstadoOrden = 2 WHERE IdOrden = " + orden;
            }
            else
            {
                update = "UPDATE ORDEN SET CantidadPagada = " + NuevaCantidadPagada + " WHERE IdOrden = " + orden;
            }


            //Modificaremos la sentencia del abono segun el metodo de pago elegido
            if (FormaPago == 1)
            {
                sentencia = "INSERT INTO ABONO VALUES(" + MontoAPagar + ", '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + FormaPago + ", " + orden + ", NULL, NULL, NULL, NULL, NULL, '" + moneda.SelectedItem.Text + "')";

            }
            else if (FormaPago == 2)
            {
                sentencia = "INSERT INTO ABONO VALUES(" + MontoAPagar + ", '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + FormaPago + ", " + orden + ", NULL, NULL, NULL, '" + Emisor.SelectedValue + "', '" + TextBox1.Text + "', '" + moneda.SelectedItem.Text + "')";

            }
            else
            {
                sentencia = "INSERT INTO ABONO VALUES(" + MontoAPagar + ", '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + FormaPago + ", " + orden + ", '" + Banco.SelectedValue + "', '" + CuentaBancaria.Text + "', '" + NoCheque.Text + "', NULL, NULL, '" + moneda.SelectedItem.Text + "')";
            }


            //Se llevaran a cabo las sentencias correspondientes
            using (SqlConnection sqlCon11 = new SqlConnection(connectionStrin))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }

            using (SqlConnection sqlCon1 = new SqlConnection(connectionStrin))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(update, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }

        }

        protected void RegistroEfectivo_Click(object sender, EventArgs e)
        {
            float MontoPagar = 0;
            string orden = NoOrden.SelectedValue;
            //Intentaremos convertir a float 
            try
            {
                MontoPagar = float.Parse(CantidadPagar.Text);
                MontoPagar = CantidadPagadaEnDolares(MontoPagar);
            }
            catch
            {
                //No haremos nada xD
            }


            if (MontoPagar > 0)
            {
                if (SePuedePagarEsaCantidad(orden, MontoPagar))
                {
                    RealizarPago(orden, 1, MontoPagar, CantidadPagadaActual(orden));

                    //Generar PDF
                    if (EsElUltimoAbono(orden))
                    {
                        RealizarFactura(orden);
                        GeneracionPDFparaFactura(orden, 1);
                    }
                    else
                    {
                        GeneracionPDFReciboPago(1, orden, CantidadPagar.Text);
                    }

                }
                else
                {
                    AvisosEfectivo.Text = "La Cantidad excede el monto faltante";
                }
            }
            else
            {
                AvisosEfectivo.Text = "Ingrese un valor numerico mayor a cero";
            }
        }

        protected void RegistroTarjeta_Click(object sender, EventArgs e)
        {
            float MontoPagar = 0;
            string orden = NoOrden.SelectedValue;
            //Intentaremos convertir a float 
            try
            {
                MontoPagar = float.Parse(CantidadPagar.Text);
                MontoPagar = CantidadPagadaEnDolares(MontoPagar);
            }
            catch
            {
                //No haremos nada xD
            }


            if (!TextBox1.Text.Equals(""))
            {
                if (MontoPagar > 0)
                {
                    if (SePuedePagarEsaCantidad(orden, MontoPagar))
                    {
                        RealizarPago(orden, 2, MontoPagar, CantidadPagadaActual(orden));
                        //Generar PDF
                        if (EsElUltimoAbono(orden))
                        {
                            RealizarFactura(orden);
                            GeneracionPDFparaFactura(orden, 2);
                        }
                        else
                        {
                            GeneracionPDFReciboPago(2, orden, CantidadPagar.Text);
                        }
                    }
                    else
                    {
                        AvisosTarjetaCredito.Text = "La Cantidad excede el monto faltante";
                    }
                }
                else
                {
                    AvisosTarjetaCredito.Text = "Ingrese un valor numerico mayor a cero";
                }
            }
            else
            {
                AvisosTarjetaCredito.Text = "Campo Requerido";
            }
        }

        protected void RegistroCheque_Click(object sender, EventArgs e)
        {
            float MontoPagar = 0;
            string orden = NoOrden.SelectedValue;
            //Intentaremos convertir a float 
            try
            {
                MontoPagar = float.Parse(CantidadPagar.Text);
                MontoPagar = CantidadPagadaEnDolares(MontoPagar);
            }
            catch
            {
                //No haremos nada xD
            }


            if (!((CuentaBancaria.Text.Equals("")) || (NoCheque.Text.Equals(""))))
            {
                if (MontoPagar > 0)
                {
                    if (SePuedePagarEsaCantidad(orden, MontoPagar))
                    {
                        RealizarPago(orden, 3, MontoPagar, CantidadPagadaActual(orden));
                        //Generar PDF
                        if (EsElUltimoAbono(orden))
                        {
                            RealizarFactura(orden);
                            GeneracionPDFparaFactura(orden, 3);
                        }
                        else
                        {
                            GeneracionPDFReciboPago(3, orden, CantidadPagar.Text);
                        }
                    }
                    else
                    {
                        AvisosCheque.Text = "La Cantidad excede el monto faltante";
                    }
                }
                else
                {
                    AvisosCheque.Text = "Ingrese un valor numerico mayor a cero";
                }
            }
            else
            {
                AvisosCheque.Text = "Campos Requeridos";
            }
        }

        protected void GeneracionPDFReciboPago(int MetodoPago, string orden, string CantidadPagada)
        {
            string nombreCliente = "";
            string direccionCliente = "";
            string clienteOrden = "";
            string empleadoNombre = "";
            float SaldoPendiente = 0;





            //Obtencion de datos de la orden
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT ClienteOrden, TotalAPagar - CantidadPagada FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    clienteOrden = lector.GetString(0);
                    SaldoPendiente = lector.GetFloat(1);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion de datos de la orden



            //Obtencion datos cliente
            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();
            SqlParameter parnomu = new SqlParameter("@cliente", clienteOrden);

            SqlCommand comando1 = new SqlCommand("SELECT Nombres, Direccion FROM CLIENTE WHERE NITCliente =  @cliente", sqlCon1);
            comando1.Parameters.Add(parnomu);

            try
            {
                SqlDataReader lector1 = comando1.ExecuteReader();
                while (lector1.Read())
                {
                    nombreCliente = lector1.GetString(0);
                    direccionCliente = lector1.GetString(1);
                }
                sqlCon1.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos cliente


            //Obtencion datos aprobador
            SqlConnection sqlCon21 = new SqlConnection(connectionString);
            sqlCon21.Open();
            SqlParameter parno = new SqlParameter("@empleado", WebForm1.NITusuario);

            SqlCommand comando21 = new SqlCommand("SELECT Nombres, Apellidos FROM EMPLEADO WHERE NITEmpleado = @empleado", sqlCon21);
            comando21.Parameters.Add(parno);

            try
            {
                SqlDataReader lector21 = comando21.ExecuteReader();
                while (lector21.Read())
                {
                    empleadoNombre = lector21.GetString(0) + " " + lector21.GetString(1);

                }
                sqlCon21.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos aprobador


            Document document1 = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document1, HttpContext.Current.Response.OutputStream);
            document1.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document1.Add(new Paragraph(20, "Recibo de pago, Orden No." + orden, fontTitle));
            document1.Add(new Chunk("\n"));

            Phrase p1 = new Phrase();
            p1.Add(new Chunk("Cliente: " + clienteOrden + ", " + nombreCliente + ", direccion: " + direccionCliente + "\n"));
            p1.Add(new Chunk("Vendedor: " + WebForm1.NITusuario + ", " + empleadoNombre + ", Puesto: Gerente" + "\n"));
            p1.Add(new Chunk("Valor de este pago: $" + CantidadPagadaEnDolares(float.Parse(CantidadPagar.Text)) + "\n"));
            p1.Add(new Chunk("Saldo pendiente: $" + SaldoPendiente + "\n"));
            string moned = moneda.SelectedItem.Text;
            p1.Add(new Chunk("\n"));
            p1.Add(new Chunk("Moneda de pago: " + moned + "\n"));
            p1.Add(new Chunk("Tasa de cambio: " + moneda.SelectedValue + "\n"));
            p1.Add(new Chunk("Saldo pagado en la moneda: " + moned + CantidadPagar.Text + "\n"));
            p1.Add(new Chunk("Saldo pendiente : " + moned + (SaldoPendiente / float.Parse(moneda.SelectedValue)) + "\n"));
            p1.Add(new Chunk("\n"));

            p1.Add(new Chunk("Detalle de pago: " + "\n"));

            if (MetodoPago == 1)
            {
                p1.Add(new Chunk("Efectivo " + "\n"));
            }
            else if (MetodoPago == 2)
            {
                p1.Add(new Chunk("Tarjeta de credito: " + "\n"));
                p1.Add(new Chunk("Emisor: " + Emisor.SelectedValue + "\n"));
                p1.Add(new Chunk("Numero de autorizacion: " + TextBox1.Text + "\n"));
            }
            else
            {
                p1.Add(new Chunk("Pago por cheque: " + "\n"));
                p1.Add(new Chunk("Banco: " + Banco.SelectedValue + "\n"));
                p1.Add(new Chunk("Cuenta bancaria: " + CuentaBancaria.Text + "\n"));
                p1.Add(new Chunk("Numero de cheque: " + NoCheque.Text + "\n"));
            }

            document1.Add(p1);

            document1.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=ReciboDePago" + orden + ".pdf");
            HttpContext.Current.Response.Write(document1);
            Response.Flush();
            Response.End();

        }

        //Comprobaciones para saber si  ya se imprime una factura
        protected Boolean EsElUltimoAbono(string orden)
        {
            float faltante = -1;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);

            SqlCommand comando = new SqlCommand("SELECT TotalAPagar - CantidadPagada AS Faltante FROM ORDEN WHERE IdOrden = @orden", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    faltante = lector.GetFloat(0);
                }
                sqlCon.Close();
            }
            catch
            {
                //Pondremos un aviso en el puto label mierda que muestra las advertencias
            }

            if (faltante == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void RealizarFactura(string orden)
        {
            string update = "UPDATE ORDEN SET  EstadoOrden = 3, FechaCancelada = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE IdOrden = " + orden;
            string Factura = "INSERT INTO FACTURA VALUES(1010, '" + DateTime.Now.ToString("yyyy/MM/dd") + "', " + orden + ")";

            //Se llevaran a cabo las sentencias correspondientes
            using (SqlConnection sqlCon11 = new SqlConnection(connectionStrin))
            {
                sqlCon11.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(Factura, sqlCon11);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon11.Close();
            }

            using (SqlConnection sqlCon1 = new SqlConnection(connectionStrin))
            {
                sqlCon1.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(update, sqlCon1);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon1.Close();
            }
        }

        public DataTable dtProductos(string orden)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT CARRITO.CodigoProducto as Codigo, PRODUCTO.Nombre, INVENTARIO.Precio, CARRITO.Cantidad, CARRITO.Cantidad * INVENTARIO.Precio AS PrecioTotal FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = " + orden + " JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto", conn);

                sqlDa.Fill(dt);


            }
            return dt;
        }

        protected void GeneracionPDFparaFactura(string orden, int MetodoPago)
        {
            string nombreCliente = "";
            string direccionCliente = "";
            string clienteOrden = "";
            string TelefonoCliente = "";
            float totalOrden = 0;
            string empleadoNombre = "";
            int NoFactura = 0;

            //Obtencion de datos de la orden
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@estado", orden);

            SqlCommand comando = new SqlCommand("SELECT ClienteOrden, TotalAPagar FROM ORDEN WHERE IdOrden = @estado", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    clienteOrden = lector.GetString(0);
                    totalOrden = lector.GetFloat(1);
                }
                sqlCon.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion de datos de la orden


            //Obtencion datos cliente
            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();
            SqlParameter parnomu = new SqlParameter("@cliente", clienteOrden);

            SqlCommand comando1 = new SqlCommand("SELECT Nombres, Direccion, Celular FROM CLIENTE WHERE NITCliente =  @cliente", sqlCon1);
            comando1.Parameters.Add(parnomu);

            try
            {
                SqlDataReader lector1 = comando1.ExecuteReader();
                while (lector1.Read())
                {
                    nombreCliente = lector1.GetString(0);
                    direccionCliente = lector1.GetString(1);
                    TelefonoCliente = lector1.GetString(2);
                }
                sqlCon1.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos cliente

            //Obtencion datos aprobador
            SqlConnection sqlCon21 = new SqlConnection(connectionString);
            sqlCon21.Open();
            SqlParameter parno = new SqlParameter("@empleado", WebForm1.NITusuario);

            SqlCommand comando21 = new SqlCommand("SELECT Nombres, Apellidos FROM EMPLEADO WHERE NITEmpleado = @empleado", sqlCon21);
            comando21.Parameters.Add(parno);

            try
            {
                SqlDataReader lector21 = comando21.ExecuteReader();
                while (lector21.Read())
                {
                    empleadoNombre = lector21.GetString(0) + " " + lector21.GetString(1);

                }
                sqlCon21.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion datos aprobador



            //Obtencion del numero de factura
            SqlConnection sqlCon211 = new SqlConnection(connectionString);
            sqlCon211.Open();
            SqlParameter parn = new SqlParameter("@orden", orden);

            SqlCommand comando211 = new SqlCommand("SELECT NoFactura FROM FACTURA WHERE OrdenGenerada = @orden", sqlCon211);
            comando211.Parameters.Add(parn);

            try
            {
                SqlDataReader lector211 = comando211.ExecuteReader();
                while (lector211.Read())
                {
                    NoFactura = lector211.GetInt32(0);

                }
                sqlCon211.Close();
            }
            catch
            {
                //AvisosOrdenesSubAlterno.Text = "Este empleado no tiene un Jefe";
            }
            //Obtencion del numero de factura


            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Factura NO. " + NoFactura + " de la Orden No." + orden, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("Datos de la empresa:" + "\n"));
            p.Add(new Chunk("NIT: " + NITEmpresa + "\n"));
            p.Add(new Chunk("Nombre: " + NombreEmpresa + "\n"));
            p.Add(new Chunk("Direccion: " + DireccionEmpresa + "\n"));
            p.Add(new Chunk("Telefono: " + TelefonoEmpresa + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Datos de cliente:" + "\n"));
            p.Add(new Chunk("NIT: " + clienteOrden + "\n"));
            p.Add(new Chunk("Nombre: " + nombreCliente + "\n"));
            p.Add(new Chunk("Direccion: " + direccionCliente + "\n"));
            p.Add(new Chunk("Telefono: " + TelefonoCliente + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Monto de la Factura: $" + totalOrden + "\n"));
            p.Add(new Chunk("Fecha de emision de la Factura: " + DateTime.Now.ToString("dd/MM/yyyy") + "\n"));
            p.Add(new Chunk("Atendido por: " + WebForm1.NITusuario + " Nombre: " + empleadoNombre + "\n"));
            p.Add(new Chunk("Detalle de articulos: " + "\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(orden);
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);

            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Paragraph(20, "Ultimo recibo de pago, Orden No." + orden, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p1 = new Phrase();
            p1.Add(new Chunk("Cliente: " + clienteOrden + ", " + nombreCliente + ", direccion: " + direccionCliente + "\n"));
            p1.Add(new Chunk("Vendedor: " + WebForm1.NITusuario + ", " + empleadoNombre + ", Puesto: Gerente" + "\n"));
            p1.Add(new Chunk("Valor de este pago: $" + CantidadPagadaEnDolares(float.Parse(CantidadPagar.Text)) + "\n"));
            p1.Add(new Chunk("Saldo pendiente: $" + 0 + "\n"));
            string moned = moneda.SelectedItem.Text;
            p1.Add(new Chunk("\n"));
            p1.Add(new Chunk("Moneda de pago: " + moned + "\n"));
            p1.Add(new Chunk("Tasa de cambio: " + moneda.SelectedValue + "\n"));
            p1.Add(new Chunk("Saldo pagado en la moneda: " + moned + CantidadPagar.Text + "\n"));
            p1.Add(new Chunk("Saldo pendiente : " + moned + 0 + "\n"));

            p1.Add(new Chunk("\n"));

            p1.Add(new Chunk("Detalle de pago: " + "\n"));

            if (MetodoPago == 1)
            {
                p1.Add(new Chunk("Efectivo " + "\n"));
            }
            else if (MetodoPago == 2)
            {
                p1.Add(new Chunk("Tarjeta de credito: " + "\n"));
                p1.Add(new Chunk("Emisor: " + Emisor.SelectedValue + "\n"));
                p1.Add(new Chunk("Numero de autorizacion: " + TextBox1.Text + "\n"));
            }
            else
            {
                p1.Add(new Chunk("Pago por cheque: " + "\n"));
                p1.Add(new Chunk("Banco: " + Banco.SelectedValue + "\n"));
                p1.Add(new Chunk("Cuenta bancaria: " + CuentaBancaria.Text + "\n"));
                p1.Add(new Chunk("Numero de cheque: " + NoCheque.Text + "\n"));
            }

            document.Add(p1);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=FacturaNo." + NoFactura + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();
        }

        protected float CantidadPagadaEnDolares(float cantidadPagada)
        {
            double cantidadDolares = cantidadPagada * float.Parse(moneda.SelectedValue);
            float cantidadAprox = (float)Math.Round(cantidadDolares, 2);
            return cantidadAprox;
        }
    }
}