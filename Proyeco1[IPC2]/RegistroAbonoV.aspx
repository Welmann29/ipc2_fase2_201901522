﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vendedor.Master" AutoEventWireup="true" CodeBehind="RegistroAbonoV.aspx.cs" Inherits="Proyeco1_IPC2_.RegistroAbonoV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: tomato;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: white;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--Cabecerra-->   
        <div class="city">
                
                    <h1 alig="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; REGISTRO DE ABONOS</h1>
                    <p alig="center">Registra los aboonos de los clientes con su correspondiente numero de orden, registrar los abonos hara que tu monto de vendido aumente, para lograr asi tus metas de venta.</p>

        </div> 
    <!--FIN CABECERA>

    <!--FORMULARIO DE RECOLECCION DE DATOS-->
    <section class="content">
        <div class="row">
            <!--ABONO EN EFECTIVO-->
            <div class="col-md-4">
                <div class="cliente">
                    <div>
                        <label>
                            ABONO DE ORDEN
                        </label>
                    </div>
                    <div>
                        <p>
                            Selecciona la orden a la que deseas abonar, ingresa tambien la cantidad que se abonara, posteriormente
                            llena los compos segun el metodo de pago a utilizar.
                        </p>
                    </div>
                    <div>
                        <p>

                            <asp:DropDownList ID="NoOrden" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            &nbsp;<asp:DropDownList ID="moneda" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:TextBox ID="CantidadPagar" runat="server" Width="236px"></asp:TextBox>

                        </p>
                    </div>
                    <div>
                        <label>
                            ABONO EN EFECTIVO
                        </label>
                    </div>
                    <div>
                        <p>
                            Si el abono se ha realizado en efectivo, no es necesario ningun dato adicional, 
                            haga click en el boton de aqui abajo para registrarlo
                        </p>
                    </div>
                    <div>
                        <p align="Center">

                            <asp:Button ID="RegistroEfectivo" runat="server" Text="Registrar Abono Efectivo" CssClass="btn btn-danger" OnClick="RegistroEfectivo_Click" />

                        </p>
                    </div>
                    <div>
                        <p>

                            <asp:Label ID="AvisosEfectivo" runat="server" ></asp:Label>

                        </p>
                    </div>
                </div>
            </div>
            <!--ABONO EN EFECTIVO-->


            <!--ABONO CON TARJETA DE CREDITO-->
            <div class="col-md-4">
                <div class="cliente">
                    <div>
                        <label>
                            ABONO CON TARJETA DE CREDITO
                        </label>
                    </div>
                    <div>
                        <p>
                            Para registrar un abono por medio de tarjeta de credito, se requiere informacion adicional,
                            llene los campos requeridos a continuacion y proceda a registrar el abono.
                        </p>
                    </div>
                    <div>
                        <label>
                            Emisor:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="Emisor" runat="server">
                            <asp:ListItem>Visa</asp:ListItem>
                            <asp:ListItem>Credomatic</asp:ListItem>
                            <asp:ListItem>Banco Promerica</asp:ListItem>
                            <asp:ListItem>Banco Industrial</asp:ListItem>
                        </asp:DropDownList>
&nbsp;</label>     </div>
                    <div>
                        <label>
                            No. Autorizacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox1" runat="server" Width="169px"></asp:TextBox>
&nbsp;</label>     </div>
                    <div>
                        <p align="Center">

                            <asp:Button ID="RegistroTarjeta" runat="server" Height="26px" Text="Registrar Abono Tarjeta Credito" CssClass="btn btn-danger" OnClick="RegistroTarjeta_Click" />

                        </p>
                    </div>
                    <div>
                        <p>

                            <asp:Label ID="AvisosTarjetaCredito" runat="server" ></asp:Label>

                        </p>
                    </div>
                </div>
            </div>
            <!--ABONO CON TAEJETA DE CREDITO-->

           <!--ABONO CON CHEQUE-->
            <div class="col-md-4">
                <div class="cliente">
                    <div>
                        <label>
                            ABONO CON CHEQUE
                        </label>
                    </div>
                    <div>
                        <p>
                            Para completar el registro de un abono efectuado con cheque se requieren que se llenen campos adicionales,
                            llene los campos solicitados y posteriormente seleccione la opcion, Registrar Abono Cheque
                        </p>
                    </div>
                    <div>
                        <label>
                            Banco:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="Banco" runat="server">
                            <asp:ListItem>Banrural</asp:ListItem>
                            <asp:ListItem>GyT</asp:ListItem>
                            <asp:ListItem>Banco Industrial</asp:ListItem>
                            <asp:ListItem>Bantrab</asp:ListItem>
                            <asp:ListItem>BAC</asp:ListItem>
                            <asp:ListItem>BAM</asp:ListItem>
                            <asp:ListItem>Agromercantil</asp:ListItem>
                        </asp:DropDownList>
&nbsp;
                        </label>
                    </div>
                    <div>
                        <label>
                            Cuenta Bancaria:&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="CuentaBancaria" runat="server" Width="192px"></asp:TextBox>
&nbsp;
                        </label>
                    </div>
                    <div>
                        <label>
                            No. Cheque:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="NoCheque" runat="server" Width="194px"></asp:TextBox>
&nbsp;
                        </label>
                    </div>
                    <div>
                        <p align="Center">

                            <asp:Button ID="RegistroCheque" runat="server" CssClass="btn btn-danger" Text="Registrar Abono Cheque" OnClick="RegistroCheque_Click" />

                        </p>
                    </div>
                    <div>
                        <p>

                            <asp:Label ID="AvisosCheque" runat="server" ></asp:Label>

                        </p>
                    </div>
                </div>
            </div>
            <!--ABONO CON CHEQUE-->


        </div>
    </section>
    <!--FIN FORMULARIO DE RECOPILACION-->


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <p>&copy; <%: DateTime.Now.Year %> - Pagina vendedor</p>
</asp:Content>
