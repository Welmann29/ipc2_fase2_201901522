﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Gerente.Master" AutoEventWireup="true" CodeBehind="RegistroClientesG.aspx.cs" Inherits="Proyeco1_IPC2_.RegistroClientesG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: black;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: whitesmoke;
            color: black;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--CABECERA-->
    <div class="city">
        <h1 style="text-align: center">REGISTRAR CLIENTES NUEVOS</h1>
    </div>
    <!-- FIN CABECERA-->

    <section class="content">
        <div class="row">
            <!--FORMULARIO DE REGISTRO DE CLIENTES-->
            <div class="col-md-6">
                <div class="productos">
                    
                        <div class="form-group">
                            <label>NIT*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxnit" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NOMBRE DE LA ENTIDAD*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxnombre" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>APELLIDO°(Si es cliente individual)</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxapellido" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>FECHA NACIMIENTO O INICIO LABORES</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxfechainicio" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>DIRECCION*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxdireccion" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NUMERO DE CELULAR*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcelular" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                   
                </div>
            </div>
            <!--FORMULARIO DE REGISTRO DE CLIENTES-->

            <!--FORMULARIO DE REGISTRO DE CLIENTES 2-->
            <div class="col-md-6">
                 <div class="productos">
                    
                        <div class="form-group">
                        <label>
                            DIAS CREDITO
                        </label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlSexo" runat="server" CssClass="form-control">
                                <asp:ListItem Value="15">Credito corto (15 dias)</asp:ListItem>
                                <asp:ListItem Value="30">Credito medio (30 dias)</asp:ListItem>
                                <asp:ListItem Value="45">Credito largo (45 dias)</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>LIMITE CREDITO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxLimiteCredito" runat="server"  CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>DEPARTAMENTO</label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="listdepartamento" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>MUNICIPIO</label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="listmunicipio" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>

                        <div class="form-group">
                            <label>NUMERO TELEFONO FIJO*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxtelefonofijo" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>CORREO ELECTRONICO*</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="boxcorreo" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                    
                </div>
                    
               
            </div>
            <!--FORMULARIO DE REGISTRO DE CLIENTES 2-->
        </div>


        <!--BOTONES-->
         <div align="center">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="btn btn-primary" Width="200px" Text="Registrar" OnClick="btnRegistrar_Click"  />
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-danger" Width="200px" Text="Cancelar" />
                    </td>
                </tr>
            </table>
        </div>
        <!--FIN DE BOTONES-->
        <div align="center">
            <label>

            <asp:Label ID="AvisosRegistro" runat="server"></asp:Label>

            </label>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
