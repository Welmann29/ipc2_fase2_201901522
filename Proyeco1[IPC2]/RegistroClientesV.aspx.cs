﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class Prueba : System.Web.UI.Page
    {
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        public Inserciones Inserciones = new Inserciones();

        protected void Page_Load(object sender, EventArgs e)
        {
            string consulta = "SELECT * FROM DEPARTAMENTO";
            string consulta1 = "SELECT * FROM MUNICIPIO";

            if (!IsPostBack)
            {
                listdepartamento.DataSource = Dset(consulta);
                listdepartamento.DataMember = "datos";
                listdepartamento.DataTextField = "NombreDepartamento";
                listdepartamento.DataValueField = "IdDepartamento";
                listdepartamento.DataBind();

                listmunicipio.DataSource = Dset(consulta1);
                listmunicipio.DataMember = "datos";
                listmunicipio.DataTextField = "NombreMunicipio";
                listmunicipio.DataValueField = "IdMunicipio";
                listmunicipio.DataBind();


            }
        }
        protected DataSet Dset(string sentencia)
        {

            SqlConnection sqlCon3 = new SqlConnection(connectionStrin);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            string FechaNacimiento = "";
            FechaNacimiento = boxfechainicio.Text;
            DateTime fecha = DateTime.Parse(FechaNacimiento);
            FechaNacimiento = fecha.ToString("yyyy/MM/dd");

            Inserciones.Cliente(boxnit.Text, boxnombre.Text, boxapellido.Text, FechaNacimiento, boxdireccion.Text, boxtelefonofijo.Text, boxcelular.Text, boxcorreo.Text, Int32.Parse(listdepartamento.SelectedValue), Int32.Parse(listmunicipio.SelectedValue), float.Parse(boxLimiteCredito.Text), Int32.Parse(ddlSexo.SelectedValue), 68);
            AvisosRegistro.Text = "Registro exitoso";
        }
    }
}