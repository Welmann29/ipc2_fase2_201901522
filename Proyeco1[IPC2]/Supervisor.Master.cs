﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string nombre = "";


            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();

            SqlParameter parnomus = new SqlParameter("@nit", WebForm1.NITusuario);

            SqlCommand comando = new SqlCommand("SELECT * FROM EMPLEADO WHERE NITEmpleado = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                nombre = lector.GetString(2);

            }
            sqlCon.Close();

            NombrePrincipal.Text = nombre;
        }
    }
}