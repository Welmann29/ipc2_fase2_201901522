﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supervisor.Master" AutoEventWireup="true" CodeBehind="VistaPrincipalS.aspx.cs" Inherits="Proyeco1_IPC2_.VistaPrincipalS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .city {
            background-color: teal;
            color: white;
            padding: 10px;
        }
        .cliente {
            background-color: gainsboro;
            color: black;
            padding: 10px;
        }
        .productos {
            background-color: ghostwhite;
            color: black;
            padding: 10px;
        }
        .metas {
            background-color: floralwhite;
            color: black;
            padding: 10px;
        }
   </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--Cabecera especial supervisor-->
     <div class="city"> 
         <h1>
            <asp:Label ID="NombrePrincipal" runat="server" ></asp:Label>
         </h1>
         <div class="row">
             <div class="col-md-4">
                 <div class="cliente">
                     <div class="form-group">
                            <h3>GERENTE</h3>
                     </div>
                     <div class="form-group">
                            <p>
                                <asp:Label ID="NombreGerente" runat="server" ></asp:Label>
                            </p>
                     </div>
                     <div class="form-group">
                            <p>
                                <asp:Label ID="NITGerente" runat="server"></asp:Label>
                            </p>
                     </div>
                 </div>
             </div>


             <div class="col-md-4">
                 <div class="city">

                 </div>
             </div>


             <div class="col-md-4">
                 <div class="cliente">
                     <div class="form-group" alig="left">
                            <h3>META ACTUAL:  $500</h3>                          
                     </div>
                     <div class="form-group" alig="left">
                            <p>MONTO VENDIDO:  $500</p>                          
                     </div>
                     <div class="form-group" alig="left">
                            <p>MONTO FALTANTE:  $500</p>                          
                     </div>
                 </div>
             </div>


         </div>
     </div>
    <!--FIN Cabecera especial supervisor-->

    <!--CUERPO DE VISTA PRINCIPAL-->
    <section class="content">
        <div class="row">
            <!--REGISTRO DE VIAJES-->
            <div class="col-md-4">
                <div class="productos">
                    
                    <div class="form-group">
                        <label>DESTINO (Opcional)</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="boxdestino" runat="server" Text="" CssClass="form-control" Width="500px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>KILOMETROS RECORRIDOS</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="boxkilometros" runat="server" Text="" CssClass="form-control" Width="500px"></asp:TextBox>
                    </div>
                    <p>
                        <a class="btn btn-default" runat="server" href="#">Registrar viaje &raquo;</a>
                    </p>
                </div>
            </div>
            <!--REGISTRO DE VIAJES FIN-->

            <!--CONTROL DE VEHICULO-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <label>NUMERO DE PLACA VEHICULO ASIGNADO</label>
                    </div>
                    <div class="form-group">
                        <p>se mostrara sql 4585249</p>
                    </div>
                    <div class="form-group">
                        <label>MODELO VEHICULO ASIGNADO</label>
                    </div>
                    <div class="form-group">
                        <p>se mostrara sql MAZDA</p>
                    </div>
                    <div class="form-group">
                        <label>KILOMETROS RECORRIDOS EN EL MES</label>
                    </div>
                    <div class="form-group">
                        <p>se mostrara sql 1000</p>
                    </div>
                </div>
            </div>
            <!--CONTROL DE VEHICULO FIN-->

            <!--CONTROL RECORRIDO MESES ANTERIORES-->
            <div class="col-md-4">
                <div class="productos">
                    <div class="form-group">
                        <h3>KILOMETRAJE MESES ANTERIORES</h3>
                    </div>
                    <div class="form-group">
                        <label>MES MARZO</label>
                    </div>
                    <div class="form-group">
                        <p>se mostrara sql 4585249</p>
                    </div>
                    <div class="form-group">
                        <label>MES FEBRERO</label>
                    </div>
                    <div class="form-group">
                        <p>se mostrara sql 4585249</p>
                    </div>
                </div>
            </div>
            <!--CONTROL RECORRIDO MESES ANTERIORES FIN-->

            <!--SECCION DE METAS ANTERIORES-->
            <div class="col-md-4">
                <div class="metas">
                    <h2>REPORTES DE VENTAS</h2>

                    <div>
                        <label>
                            REPORTE DE VENTAS VS META
                        </label>
                    </div>
                    <div>
                        <p>
                            Se te proporcionara un reporte en el cual se compararan las ventas totales del mes, con la meta asignada para el mes.
                        </p>
                    </div>
                    <div>
                        <label>Seleccione el mes y año para su reporte</label>
                    </div>
                    <div>
                        <p align="Center">

                            <asp:DropDownList ID="MesVvM" runat="server">
                                <asp:ListItem Value="01">Enero</asp:ListItem>
                                <asp:ListItem Value="02">Febrero</asp:ListItem>
                                <asp:ListItem Value="03">Marzo</asp:ListItem>
                                <asp:ListItem Value="04">Abril</asp:ListItem>
                                <asp:ListItem Value="05">Mayo</asp:ListItem>
                                <asp:ListItem Value="06">Junio</asp:ListItem>
                                <asp:ListItem Value="07">Julio</asp:ListItem>
                                <asp:ListItem Value="08">Agosto</asp:ListItem>
                                <asp:ListItem Value="09">Septiembre</asp:ListItem>
                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="AñoVvM" runat="server">
                                <asp:ListItem>2020</asp:ListItem>
                                <asp:ListItem>2021</asp:ListItem>
                                <asp:ListItem>2022</asp:ListItem>
                                <asp:ListItem>2023</asp:ListItem>
                                <asp:ListItem>2024</asp:ListItem>
                            </asp:DropDownList>

                        </p>
                    </div>
                    <div>

                        <p align="Center">

                            <asp:Button ID="VentasVsMetas" runat="server" CssClass="btn btn-danger" Text="Generar Reporte" OnClick="VentasVsMetas_Click" />

                        </p>
                    </div>
                    <div>
                        <label>
                            REPORTE DE VENTAS VS META x CATEGORIA
                        </label>
                    </div>
                    <div>
                        <p>
                            Se proporcionara un reporte que incluira el monto que has vendido de cada categoria comparandolo con la meta que tenias de esa categoria 
                        </p>
                    </div>
                    <div>
                        <label>Seleccione el mes y año para su reporte</label>
                    </div>
                    <div>
                        <p align="Center">

                            <asp:DropDownList ID="MesVvMxC" runat="server">
                                <asp:ListItem Value="01">Enero</asp:ListItem>
                                <asp:ListItem Value="02">Febrero</asp:ListItem>
                                <asp:ListItem Value="03">Marzo</asp:ListItem>
                                <asp:ListItem Value="04">Abril</asp:ListItem>
                                <asp:ListItem Value="05">Mayo</asp:ListItem>
                                <asp:ListItem Value="06">Junio</asp:ListItem>
                                <asp:ListItem Value="07">Julio</asp:ListItem>
                                <asp:ListItem Value="08">Agosto</asp:ListItem>
                                <asp:ListItem Value="09">Septiembre</asp:ListItem>
                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="AñoVvMxC" runat="server">
                                <asp:ListItem>2020</asp:ListItem>
                                <asp:ListItem>2021</asp:ListItem>
                                <asp:ListItem>2022</asp:ListItem>
                                <asp:ListItem>2023</asp:ListItem>
                                <asp:ListItem>2024</asp:ListItem>
                            </asp:DropDownList>

                        </p>
                    </div>
                    <div>
                        <p align="Center">

                            <asp:Button ID="VentaVsMetaxCategoria" runat="server" CssClass="btn btn-danger" Text="Generar Reporte" OnClick="VentaVsMetaxCategoria_Click" />

                        </p>
                    </div>
                </div>
            </div>
            <!--SECCION DE METAS ANTERIORES FIN-->

            <!--SECCION DE ACTUALIZACION DE DATOS-->
            <div class="col-md-4">
            <label>REPORTE DE VENTAS POR PRODUCTO, CATEGORIA O CLIENTE </label>
            <div class="form-group">
                <label>Ingrese la fecha inicio para el reporte:</label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="boxFechaInicio" runat="server" Text="" CssClass="form-control">01/06/2020</asp:TextBox>
            </div>
            <div class="form-group">
                <label>Ingrese la fecha final para el reporte:</label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="boxFechaFinal" runat="server" Text="" CssClass="form-control">01/07/2020</asp:TextBox>
            </div>
            <div class="form-group">
                <label>Producto o categoria para el reporte:</label>
            </div>
            <div align="Center">

                <asp:DropDownList ID="Productos" runat="server" AutoPostBack="True">
                </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;
                <asp:DropDownList ID="Categorias" runat="server" AutoPostBack="True">
                </asp:DropDownList>

            </div>
            <!--Falta añadir acciones-->
            <div align="Center">

                <asp:Button ID="PorProducto" runat="server" Text="Reporte Producto" OnClick="PorProducto_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;
                <asp:Button ID="PorCategoria" runat="server" Text="Reporte Categoria" OnClick="PorCategoria_Click" />

            </div>

            <div>
                <label>
                    Cliente para el reporte
                </label>
            </div>
            <div align="center">

                <asp:DropDownList ID="Clientes" runat="server">
                </asp:DropDownList>

            </div>
            <div align="Center">

                <asp:Button ID="PorCliente" runat="server" Text="Reporte Cliente" OnClick="PorCliente_Click" />

            </div>

        <!--Añadir conexiones para mostrar datos del usuario-->
        </div>
            <!--SECCION DE ACTUALIZACION DE DATOS FIN-->

            <!--SECCION DE DATOS PERSONALES-->
            <div class="col-md-4">
                <div class="metas">
                    <div class="form-group">
                        <h3>DATOS PERSONALES</h3>
                    </div>
                    <div class="form-group">
                        <label>NIT empleado (ID):</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="NitEmpleado" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>Nombre completo:</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="nombreE" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>NUmero de celular:</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="numcelular" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>NUmero de telefono (Linea fija):</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="numtelefono" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>Correo Electronico:</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="correo" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>Fecha de nacimietno:</label>
                    </div>
                    <div class="form-group">
                        <p align="center"><asp:Label ID="nacimiento" runat="server" ></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
            <!--SECCION DE DATOS PERSONALES FIN-->
        </div>
    </section>
    <!--CUERPO DE VISTA PRINCIPAL FIN--> 


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
