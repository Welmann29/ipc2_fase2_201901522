﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyeco1_IPC2_
{
    public partial class VistaPrincipalV : System.Web.UI.Page
    {
        protected string nombre = "";
        protected string direccion = "";
        public string connectionStrin = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        protected void Page_Load(object sender, EventArgs e)
        {

            
            string celular = "";
            string telefono = "";
            string email = "";
            string nacimientor = "";
            string nitsuperior = "";
            string nombresuperior = "";
            string SentenciaProductos = "SELECT CodigoProducto, Nombre FROM PRODUCTO";
            string SentenciaCategorias = "SELECT * FROM CATEGORIA";
            string SentenciaClientes = "SELECT NITCliente, Nombres + ' ' + Apellidos as Nombre FROM CLIENTE";

            if (!IsPostBack)
            {
                Productos.DataSource = Dset(SentenciaProductos);
                Productos.DataMember = "datos";
                Productos.DataTextField = "Nombre";
                Productos.DataValueField = "CodigoProducto";
                Productos.DataBind();

                Categorias.DataSource = Dset(SentenciaCategorias);
                Categorias.DataMember = "datos";
                Categorias.DataTextField = "NombreCategoria";
                Categorias.DataValueField = "IdCategoria";
                Categorias.DataBind();

                Clientes.DataSource = Dset(SentenciaClientes);
                Clientes.DataMember = "datos";
                Clientes.DataTextField = "Nombre";
                Clientes.DataValueField = "NITCliente";
                Clientes.DataBind();
            }

            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();

            SqlParameter parnomus = new SqlParameter("@nit", WebForm1.NITusuario);

            SqlCommand comando = new SqlCommand("SELECT * FROM EMPLEADO WHERE NITEmpleado = @nit", sqlCon);
            comando.Parameters.Add(parnomus);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                nitsuperior = lector.GetString(1);
                nombre = lector.GetString(2) + " " + lector.GetString(3);
                direccion = lector.GetString(4);
                celular = lector.GetString(5);
                telefono = lector.GetString(6);
                email = lector.GetString(7);
                nacimientor = lector.GetDateTime(8).ToString("MM/dd/yyyy");
            }
            sqlCon.Close();

            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();

            SqlParameter parnomu = new SqlParameter("@nitsuperior", nitsuperior);

            SqlCommand comando1 = new SqlCommand("SELECT Nombres, Apellidos FROM EMPLEADO WHERE NITEmpleado = @nitsuperior", sqlCon1);
            comando1.Parameters.Add(parnomu);

            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read()) {
                nombresuperior = lector1.GetString(0) + " " + lector1.GetString(1);
            }

            sqlCon1.Close();

            NombrePrincipal.Text = nombre;
            NitEmpleado.Text = WebForm1.NITusuario;
            nombreE.Text = nombre;
            numcelular.Text = celular;
            numtelefono.Text = telefono;
            correo.Text = email;
            nacimiento.Text = nacimientor;
            NombreSupervisor.Text = nombresuperior;
            NITSupervisor.Text = nitsuperior;
        }
        protected DataSet Dset(string sentencia)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            SqlConnection sqlCon3 = new SqlConnection(connectionString);
            DataSet ds = new DataSet();
            sqlCon3.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, sqlCon3);

            sqlDa.Fill(ds, "datos");

            sqlCon3.Close();
            return ds;
        }

        protected int ObtencionIdMeta(string mes, string año, string nit) {
            int IdMeta = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@nit", nit);

            SqlCommand comando = new SqlCommand("SELECT IdMeta FROM METAMES WHERE METAMES.NITEmpleado = @nit AND FechaInicio = '" + año + "/" + mes + "/01'", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    IdMeta = lector.GetInt32(0);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return IdMeta;
        }
        //Generacion del PDF Reporte de VentasvsMeta Simple
        protected void GeneracionReporteVentasVsMeta(string mes, string año, string nit) {
            double MetaMes = 0;
            double TotalOrdenesCerradas = 0;
            double TotalOrdenesPagadas = 0;
            double OrdenesAPagar = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@IdMeta", ObtencionIdMeta(mes, año, nit));

            SqlCommand comando = new SqlCommand("SELECT SUM(METACATEGORIA.Monto) as MetaTotal FROM METACATEGORIA WHERE IdMeta = @IdMeta", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    MetaMes = lector.GetDouble(0);
                }
                sqlCon.Close();
            }
            catch {
                sqlCon.Close();
            }


            sqlCon.Open();
            SqlParameter parnomu = new SqlParameter("@nit", nit);

            SqlCommand comand = new SqlCommand("SELECT SUM(TotalAPagar) as Total FROM ORDEN WHERE EstadoOrden = 3 AND EmpleadoOrden = @nit AND MONTH(FechaCancelada) = " + mes + " AND YEAR(FechaCancelada) = " + año, sqlCon);
            comand.Parameters.Add(parnomu);

            try
            {
                SqlDataReader lecto = comand.ExecuteReader();
                while (lecto.Read())
                {
                    TotalOrdenesPagadas = lecto.GetDouble(0);
                }
                sqlCon.Close();
            }
            catch {
                sqlCon.Close();
            }

            sqlCon.Open();
            SqlParameter parnom = new SqlParameter("@nit", nit);

            SqlCommand coman = new SqlCommand("SELECT SUM(TotalAPagar) as Total FROM ORDEN WHERE EstadoAprobacion > 4 AND EmpleadoOrden = @nit AND MONTH(FechaCerrada) = " + mes + " AND YEAR(FechaCerrada) = " + año, sqlCon);
            coman.Parameters.Add(parnom);

            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    TotalOrdenesCerradas = lect.GetDouble(0);
                }
                sqlCon.Close();
            }
            catch {
                sqlCon.Close();
            }


            sqlCon.Open();
            SqlParameter parno = new SqlParameter("@nit", nit);

            SqlCommand coma = new SqlCommand("SELECT SUM(TotalAPagar) as Total FROM ORDEN WHERE EstadoOrden < 3 AND EstadoAprobacion = 6 AND EmpleadoOrden = @nit AND MONTH(FechaCerrada) = " + mes + " AND YEAR(FechaCerrada) = " + año, sqlCon);
            coma.Parameters.Add(parno);

            try
            {
                SqlDataReader lec = coma.ExecuteReader();
                while (lec.Read())
                {
                    OrdenesAPagar = lec.GetDouble(0);
                }
                sqlCon.Close();
            }
            catch {
                sqlCon.Close();
            }

            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Reporte de Ventas vs Meta " + mes + "/" + año, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("Reporte del empleado: " + "\n"));
            p.Add(new Chunk("NIT:" + nit + "\n"));
            p.Add(new Chunk("Nombre completo: " + nombre + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Mes que se reporta: " + MesVvM.Text + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Fecha y hora del reporte: " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm") + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Meta del Mes: $" + MetaMes + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes Cerradas: $" + TotalOrdenesCerradas + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes pagadas: $" + TotalOrdenesPagadas + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes por pagar: $" + OrdenesAPagar + "\n"));
            p.Add(new Chunk("\n"));
            double cumplido = (TotalOrdenesPagadas / MetaMes) * 100;
            if (cumplido > 100) {
                cumplido = 100;
            }
            p.Add(new Chunk("Porcentaje cumplido: " + (cumplido).ToString("N4") + "%" + "\n"));
            document.Add(p);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Reporte de Ventas vs Meta " + mes + " / " + año + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();

        }

        protected void VentasVsMetas_Click(object sender, EventArgs e)
        {

            GeneracionReporteVentasVsMeta(MesVvM.SelectedValue, AñoVvM.SelectedValue, WebForm1.NITusuario);

        }


        //Recupera el monto de la categoria en una orden especifica.
        protected float MontoCategoria(int orden, int idCategoria) {
            float monto = 0;
            float posibleMonto = 0;
            int idCategoriaComprobador = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnomus = new SqlParameter("@orden", orden);

            SqlCommand comando = new SqlCommand("SELECT  CARRITO.Cantidad * INVENTARIO.Precio AS PrecioTotal, PRODUCTO.IdCategoria FROM CARRITO JOIN INVENTARIO  ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND IdOrden = @orden JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto ", sqlCon);
            comando.Parameters.Add(parnomus);

            try
            {
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    posibleMonto = lector.GetFloat(0);
                    idCategoriaComprobador = lector.GetInt32(1);
                    if (idCategoria == idCategoriaComprobador) {
                        monto = monto + posibleMonto;
                    }

                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return monto;
        }

        protected float MontoOrdenesCerradas(string mes, string año, string nit, int idCategoria) {
            float MontoOrdenesCerradas = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnom = new SqlParameter("@nit", nit);

            SqlCommand coman = new SqlCommand("SELECT IdOrden FROM ORDEN WHERE EstadoAprobacion > 4 AND EmpleadoOrden = @nit AND MONTH(FechaCerrada) = " + mes + " AND YEAR(FechaCerrada) = " + año, sqlCon);
            coman.Parameters.Add(parnom);

            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    MontoOrdenesCerradas = MontoOrdenesCerradas + MontoCategoria(lect.GetInt32(0), idCategoria);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return MontoOrdenesCerradas;
        }

        protected float MontoOrdenesPagadas(string mes, string año, string nit, int idCategoria) {
            float MontoOrdenesPagadas = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnom = new SqlParameter("@nit", nit);

            SqlCommand coman = new SqlCommand("SELECT IdOrden FROM ORDEN WHERE EstadoOrden = 3 AND EmpleadoOrden = @nit AND MONTH(FechaCancelada) = " + mes + " AND YEAR(FechaCancelada) = " + año, sqlCon);
            coman.Parameters.Add(parnom);

            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    MontoOrdenesPagadas = MontoOrdenesPagadas + MontoCategoria(lect.GetInt32(0), idCategoria);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return MontoOrdenesPagadas;
        }

        protected float MontoTotalAPAgar(string mes, string año, string nit, int idCategoria) {
            float MontoTotalAPagar = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnom = new SqlParameter("@nit", nit);

            SqlCommand coman = new SqlCommand("SELECT IdOrden FROM ORDEN WHERE EstadoOrden < 3 AND EstadoAprobacion = 6 AND EmpleadoOrden = @nit AND MONTH(FechaCerrada) = " + mes + " AND YEAR(FechaCerrada) = " + año, sqlCon);
            coman.Parameters.Add(parnom);

            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    MontoTotalAPagar = MontoTotalAPagar + MontoCategoria(lect.GetInt32(0), idCategoria);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return MontoTotalAPagar;
        }

        protected float MetaCategoria(int idMeta, int idCategoria) {
            float MetaCategoria = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlParameter parnom = new SqlParameter("@idMeta", idMeta);

            SqlCommand coman = new SqlCommand("SELECT Monto FROM METACATEGORIA WHERE IdMeta = @idMeta AND IdCategoria = " + idCategoria, sqlCon);
            coman.Parameters.Add(parnom);

            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    MetaCategoria = lect.GetFloat(0);
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            return MetaCategoria;
        }

        protected Phrase parrafoCategoria(string mes, string año, string nit, int idCategoria) {
            Phrase p = new Phrase();
            p.Add(new Chunk("Meta del Mes: $" + MetaCategoria(ObtencionIdMeta(mes, año, nit), idCategoria) + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes Cerradas: $" + MontoOrdenesCerradas(mes, año, nit, idCategoria) + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes pagadas: $" + MontoOrdenesPagadas(mes, año, nit, idCategoria) + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Total Ordenes por pagar: $" + MontoTotalAPAgar(mes, año, nit, idCategoria) + "\n"));
            p.Add(new Chunk("\n"));
            double cumplido = (MontoOrdenesPagadas(mes, año, nit, idCategoria) / MetaCategoria(ObtencionIdMeta(mes, año, nit), idCategoria)) * 100;
            if (cumplido > 100)
            {
                cumplido = 100;
            }
            p.Add(new Chunk("Porcentaje cumplido: " + (cumplido).ToString("N4") + "%" + "\n"));
            return p;
        }

        protected void GeneracionReporteVentaVsMetaxCategoria(string mes, string año, string nit) {
            int idCategoria = 0;
            string nombreCategoria = "";
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Reporte de Ventas vs MetaxCategoria " + mes + "/" + año, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("Reporte del empleado: " + "\n"));
            p.Add(new Chunk("NIT:" + nit + "\n"));
            p.Add(new Chunk("Nombre completo: " + nombre + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Mes que se reporta: " + MesVvMxC.Text + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Fecha y hora del reporte: " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm") + "\n"));
            p.Add(new Chunk("\n"));

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();


            SqlCommand coman = new SqlCommand("SELECT * FROM CATEGORIA", sqlCon);


            try
            {
                SqlDataReader lect = coman.ExecuteReader();
                while (lect.Read())
                {
                    idCategoria = lect.GetInt32(0);
                    nombreCategoria = lect.GetString(1);

                    p.Add(new Chunk("Categoria: " + idCategoria + " " + nombreCategoria + "--------------------------------------" + "\n"));
                    p.Add(parrafoCategoria(mes, año, nit, idCategoria));
                }
                sqlCon.Close();
            }
            catch
            {
                sqlCon.Close();
            }
            document.Add(p);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Reporte de Ventas vs MetaxCategoria " + mes + " / " + año + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();
        }

        protected void VentaVsMetaxCategoria_Click(object sender, EventArgs e)
        {
            GeneracionReporteVentaVsMetaxCategoria(MesVvMxC.SelectedValue, AñoVvMxC.SelectedValue, WebForm1.NITusuario);
        }

        //Reportes de la Fase 3

        public DataTable dtProductos(string sentencia)
        {
            string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                SqlDataAdapter sqlDa = new SqlDataAdapter(sentencia, conn);

                sqlDa.Fill(dt);

            }
            return dt;
        }

        public string SentenciaPorProductos(string codigoProducto, string FechaInicio, string FechaFinal) {
            string sentencia = "SELECT ORDEN.FechaCreacion AS 'Fecha orden', CARRITO.IdOrden AS 'Codigo orden', CARRITO.Cantidad, INVENTARIO.Precio AS 'Precio ($)', CARRITO.Cantidad* INVENTARIO.Precio AS 'Total($)' FROM ORDEN JOIN CARRITO ON CARRITO.IdOrden = ORDEN.IdOrden AND CARRITO.CodigoProducto = " + codigoProducto + " JOIN INVENTARIO ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND ORDEN.FechaCreacion BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "'";
            return sentencia;
        }

        public void PDFporProducto(string CodigoProducto, string FechaInicio, string FechaFinal) {
            string nombreProducto = "";
            string descripcion = "";
            int CantidadVentasTotal = 0;
            float DineroTotal = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
      
            SqlCommand comando = new SqlCommand("SELECT * FROM PRODUCTO WHERE CodigoProducto = " + CodigoProducto, sqlCon);
           
          
                SqlDataReader lector = comando.ExecuteReader();
                while (lector.Read())
                {
                    nombreProducto = lector.GetString(1);
                try
                {
                    descripcion = lector.GetString(2);
                }
                catch {
                    descripcion = "Sin Descripcion";
                }
                }
                sqlCon.Close();
            
                
            
           

            sqlCon.Open();

            

            SqlCommand comando1 = new SqlCommand(SentenciaPorProductos(CodigoProducto, FechaInicio, FechaFinal), sqlCon);

            
                SqlDataReader lector1 = comando1.ExecuteReader();
                while (lector1.Read())
                {
                    CantidadVentasTotal = CantidadVentasTotal + lector1.GetInt32(2);
                    DineroTotal = DineroTotal + lector1.GetFloat(4);
                }
                sqlCon.Close();
           
           
            

            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Reporte de Ventas del producto " + CodigoProducto, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Producto: " + "\n"));
            p.Add(new Chunk("Codigo:" + CodigoProducto + "\n"));
            p.Add(new Chunk("Descripcion: " + descripcion + "\n"));
            p.Add(new Chunk("\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(SentenciaPorProductos(CodigoProducto, FechaInicio, FechaFinal));
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);

            Phrase p1 = new Phrase();
            p1.Add(new Chunk("\n"));
            p1.Add(new Chunk("Cantidad total vendida: " + CantidadVentasTotal + "\n"));
            p1.Add(new Chunk("Monto total vendido: $" + DineroTotal + "\n"));
          
            document.Add(p1);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=ReporteDeProductoCodigo." + CodigoProducto + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();

        }

        protected void PorProducto_Click(object sender, EventArgs e)
        {
            DateTime Inicio = DateTime.Parse(boxFechaInicio.Text);
            DateTime Final = DateTime.Parse(boxFechaFinal.Text);
            string FInicio = Inicio.ToString("yyyy/MM/dd");
            string FFinal = Final.ToString("yyyy/MM/dd");
            PDFporProducto(Productos.SelectedValue, FInicio, FFinal);
        }

        public string SentenciaPorCategoria(string CodigoCategoria, string FechaInicio, string FechaFinal) {
            string sentencia = "SELECT CARRITO.CodigoProducto AS Producto, PRODUCTO.Nombre, SUM(CARRITO.Cantidad) AS 'Cantidad Total', COUNT(CARRITO.IdOrden) AS 'Cantidad de Ordenes',  AVG(ORDEN.TotalAPagar) AS 'Promedio TotalxOrden ($)', AVG(CARRITO.Cantidad * INVENTARIO.Precio) AS 'Promedio x Orden ($)' FROM ORDEN JOIN CARRITO ON CARRITO.IdOrden = ORDEN.IdOrden JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto AND PRODUCTO.IdCategoria = " + CodigoCategoria + " JOIN INVENTARIO ON INVENTARIO.CodigoProducto = CARRITO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " AND ORDEN.FechaCreacion BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "' GROUP BY CARRITO.CodigoProducto, PRODUCTO.Nombre";
            return sentencia;
        }

        public void PDFporCategoria(string CodigoCategoria, string FechaInicio, string FechaFinal) {
            string nombreCategoria = "";
            int cantidadOrdenes = 0;
            double promedioTotal = 0;
            double promedioOrden = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();

            SqlCommand comando = new SqlCommand("SELECT * FROM CATEGORIA WHERE IdCategoria = " + CodigoCategoria, sqlCon);


            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                nombreCategoria = lector.GetString(1);
                
            }
            sqlCon.Close();

            sqlCon.Open();



            SqlCommand comando1 = new SqlCommand(SentenciaPorCategoria(CodigoCategoria, FechaInicio, FechaFinal), sqlCon);


            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                cantidadOrdenes = cantidadOrdenes + lector1.GetInt32(2);
                promedioTotal = promedioTotal + lector1.GetDouble(4);
                promedioOrden = promedioOrden + lector1.GetDouble(5);
            }
            sqlCon.Close();


            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Reporte de Ventas de la categoria " + CodigoCategoria, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Categoria: " + "\n"));
            p.Add(new Chunk("Codigo:" + CodigoCategoria + "\n"));
            p.Add(new Chunk("Nombre: " + nombreCategoria + "\n"));
            p.Add(new Chunk("\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(SentenciaPorCategoria(CodigoCategoria, FechaInicio, FechaFinal));
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);

            Phrase p1 = new Phrase();
            p1.Add(new Chunk("\n"));
            p1.Add(new Chunk("Cantidad total vendida de los productos de la categoria: " + cantidadOrdenes + "\n"));
            p1.Add(new Chunk("Monto promedio de las ordenes: $" + promedioTotal.ToString("N2") + "\n"));
            p1.Add(new Chunk("Monto promedio de los productos en las ordenes: $" + promedioOrden.ToString("N2") + "\n"));

            document.Add(p1);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=ReporteDeCategoriaCodigo." + CodigoCategoria + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();
        }

        protected void PorCategoria_Click(object sender, EventArgs e)
        {
            DateTime Inicio = DateTime.Parse(boxFechaInicio.Text);
            DateTime Final = DateTime.Parse(boxFechaFinal.Text);
            string FInicio = Inicio.ToString("yyyy/MM/dd");
            string FFinal = Final.ToString("yyyy/MM/dd");
            PDFporCategoria(Categorias.SelectedValue, FInicio, FFinal);
        }

        public string SentenciaPorCliente(string NIT, string FechaInicio, string FechaFinal) {
            string consulta = "SELECT CARRITO.CodigoProducto, PRODUCTO.Nombre, AVG(INVENTARIO.Precio) AS Precio, SUM(CARRITO.Cantidad) AS Cantidad, AVG(Precio) * SUM(Cantidad) AS Total FROM CARRITO JOIN INVENTARIO ON CARRITO.CodigoProducto = INVENTARIO.CodigoProducto AND INVENTARIO.CodigoLista = " + WebForm1.litaActual + " JOIN ORDEN ON ORDEN.IdOrden = CARRITO.IdOrden AND ORDEN.ClienteOrden = '" + NIT + "' AND ORDEN.FechaCreacion BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "' JOIN PRODUCTO ON PRODUCTO.CodigoProducto = CARRITO.CodigoProducto GROUP BY CARRITO.CodigoProducto, PRODUCTO.Nombre";
            return consulta;
        }

        public string SentenciaAnuladasDelCliente(string NIT, string FechaInicio, string FechaFinal)
        {
            string sentencia = "SELECT ORDEN.FechaCreacion AS Fecha, ORDEN.IdOrden AS 'Codigo Orden', ORDEN.TotalAPagar - ORDEN.CantidadPagada AS 'Monto en $' FROM ORDEN  WHERE EstadoOrden = 4 AND CantidadPagada > 0 AND ClienteOrden = '" + NIT + "' AND FechaCreacion BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "'";
            return sentencia;
        }

        public double MontoTotalAnulado(string NIT, string FechaInicio, string FechaFinal)
        {
            double anulado = 0;
            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlCommand comando1 = new SqlCommand(SentenciaAnuladasDelCliente(NIT, FechaInicio, FechaFinal), sqlCon);


            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                anulado = anulado + lector1.GetFloat(2);
            }
            sqlCon.Close();
            return anulado;
        }

        public string SentenciaPagosCliente(string NIT, string FechaInicio, string FechaFinal) {
            string consulta = "SELECT ABONO.FechaPago AS Fecha, ABONO.Orden, ABONO.Moneda, MONEDA.TasaCambio, ABONO.Monto AS 'Monto ($)' FROM ABONO JOIN MONEDA ON ABONO.Moneda = MONEDA.Simbolo JOIN ORDEN ON ABONO.Orden = ORDEN.IdOrden AND Orden.ClienteOrden = '" + NIT + "' AND ABONO.FechaPago BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "'";
            return consulta;
        }

        public int MontoTotalPagado(string NIT, string FechaInicio, string FechaFinal) {
            int MontoTotal = 0;

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();
            SqlCommand comando1 = new SqlCommand(SentenciaPagosCliente(NIT, FechaInicio, FechaFinal), sqlCon);


            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                MontoTotal = MontoTotal + lector1.GetInt32(4);
            }
            sqlCon.Close();
            return MontoTotal;
        }

        protected void PDFporCliente(string NIT, string FechaInicio, string FechaFinal) {
            string NombreLista = "";
            double TotalCliente = 0;
            string nombreCliente = "";
            string direccionCliente = "";

            SqlConnection sqlCon = new SqlConnection(connectionStrin);
            sqlCon.Open();

            SqlCommand comando = new SqlCommand("SELECT * FROM LISTAPRECIO WHERE CodigoLista = " + WebForm1.litaActual, sqlCon);


            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                NombreLista = lector.GetString(3);

            }
            sqlCon.Close();




            sqlCon.Open();
            SqlCommand comando1 = new SqlCommand(SentenciaPorCliente(NIT, FechaInicio, FechaFinal), sqlCon);


            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                TotalCliente = TotalCliente + lector1.GetDouble(4);
            }
            sqlCon.Close();


            sqlCon.Open();
            SqlCommand comando2 = new SqlCommand(" SELECT Nombres + ' ' + Apellidos, Direccion FROM CLIENTE WHERE NITCliente = '" + NIT + "'", sqlCon);


            SqlDataReader lector2 = comando2.ExecuteReader();
            while (lector2.Read())
            {
                nombreCliente = lector2.GetString(0);
                try
                {
                    direccionCliente = lector2.GetString(1);
                }
                catch {
                    direccionCliente = "Direccion no especificada";
                }
            }
            sqlCon.Close();

            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            Font fontTitle = FontFactory.GetFont(FontFactory.COURIER_BOLD, 25);
            Font font9 = FontFactory.GetFont(FontFactory.TIMES, 9);

            document.Add(new Paragraph(20, "Reporte de Ventas del cliente " + NIT, fontTitle));
            document.Add(new Chunk("\n"));

            Phrase p = new Phrase();
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Cliente " + "\n"));
            p.Add(new Chunk("NIT:" + NIT + "\n"));
            p.Add(new Chunk("Nombre: " + nombreCliente + "\n"));
            p.Add(new Chunk("Direccion: " + direccionCliente + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Vendedor " + "\n"));
            p.Add(new Chunk("NIT:" + WebForm1.NITusuario + "\n"));
            p.Add(new Chunk("Nombre: " + nombre + "\n"));
            p.Add(new Chunk("Direccion: " + direccion + "\n"));
            p.Add(new Chunk("Puesto: Vendedor" + "\n"));
            p.Add(new Chunk("\n"));
            p.Add(new Chunk("Precios basados en la lista actual " + "\n"));
            p.Add(new Chunk("Codigo lista:" + WebForm1.litaActual + "\n"));
            p.Add(new Chunk("Nombre de la lista: " + NombreLista + "\n"));

            p.Add(new Chunk("\n"));
            document.Add(p);

            DataTable dt = new DataTable();
            dt = dtProductos(SentenciaPorCliente(NIT, FechaInicio, FechaFinal));
            PdfPTable table = new PdfPTable(dt.Columns.Count);


            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 90;

            PdfPCell cell = new PdfPCell(new Phrase("columns"));
            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table);

            Phrase p1 = new Phrase();
            p1.Add(new Chunk("\n"));
            p1.Add(new Chunk("Monto total gastado por el cliente: $" + TotalCliente + "\n"));


            document.Add(p1);

            document.Add(new Chunk("\n"));
            document.Add(new Chunk("\n"));
            document.Add(new Chunk("Detalles de pago:" + "\n"));

            DataTable dt1 = new DataTable();
            dt1 = dtProductos(SentenciaPagosCliente(NIT, FechaInicio, FechaFinal));
            PdfPTable table1 = new PdfPTable(dt1.Columns.Count);


            float[] widths1 = new float[dt1.Columns.Count];
            for (int i = 0; i < dt1.Columns.Count; i++)
                widths1[i] = 4f;

            table1.SetWidths(widths1);
            table1.WidthPercentage = 90;

            PdfPCell cell1 = new PdfPCell(new Phrase("columns"));
            cell1.Colspan = dt1.Columns.Count;

            foreach (DataColumn c in dt1.Columns)
            {
                table1.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt1.Rows)
            {
                if (dt1.Rows.Count > 0)
                {
                    for (int h = 0; h < dt1.Columns.Count; h++)
                    {
                        table1.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table1);

            Phrase p2 = new Phrase();
            p2.Add(new Chunk("\n"));
            p2.Add(new Chunk("Monto total pagado por el cliente: $" + MontoTotalPagado(NIT, FechaInicio, FechaFinal) + "\n"));
            p2.Add(new Chunk("\n"));

            document.Add(p2);

            document.Add(new Chunk("\n"));
            document.Add(new Chunk("Detalles de nota de credito:" + "\n"));

            DataTable dt11 = new DataTable();
            dt11 = dtProductos(SentenciaAnuladasDelCliente(NIT, FechaInicio, FechaFinal));
            PdfPTable table11 = new PdfPTable(dt11.Columns.Count);


            float[] widths11 = new float[dt11.Columns.Count];
            for (int i = 0; i < dt11.Columns.Count; i++)
                widths11[i] = 4f;

            table11.SetWidths(widths11);
            table11.WidthPercentage = 90;

            PdfPCell cell11 = new PdfPCell(new Phrase("columns"));
            cell11.Colspan = dt11.Columns.Count;

            foreach (DataColumn c in dt11.Columns)
            {
                table11.AddCell(new Phrase(c.ColumnName, font9));
            }

            foreach (DataRow r in dt11.Rows)
            {
                if (dt11.Rows.Count > 0)
                {
                    for (int h = 0; h < dt11.Columns.Count; h++)
                    {
                        table11.AddCell(new Phrase(r[h].ToString(), font9));
                    }
                }
            }
            document.Add(table11);

            Phrase p21 = new Phrase();
            p21.Add(new Chunk("\n"));
            p21.Add(new Chunk("Monto total anulado por el cliente: $" + MontoTotalAnulado(NIT, FechaInicio, FechaFinal).ToString("N2") + "\n"));
            p21.Add(new Chunk("\n"));

            document.Add(p21);

            document.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=ReporteDeClienteNIT." + NIT + ".pdf");
            HttpContext.Current.Response.Write(document);
            Response.Flush();
            Response.End();
        }

        protected void PorCliente_Click(object sender, EventArgs e)
        {
            DateTime Inicio = DateTime.Parse(boxFechaInicio.Text);
            DateTime Final = DateTime.Parse(boxFechaFinal.Text);
            string FInicio = Inicio.ToString("yyyy/MM/dd");
            string FFinal = Final.ToString("yyyy/MM/dd");
            PDFporCliente(Clientes.SelectedValue, FInicio, FFinal);
        }
    }


    
}