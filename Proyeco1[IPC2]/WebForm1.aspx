﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Proyeco1_IPC2_.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INICIA SESION</h1>
        <p class="lead">Inicia sesión para verificar tus metas, generar nuevas ordenes de venta, verificar los abonos a tus ordenes y mucho más...</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h3>Usuarios DIPROMA</h3>
            <p>DIPROMA pone a la disposicion de sus empleados esta novedosa aplicacion, la cual, les permitira llevar un control mas optimo y
                sencillo de sus operaciones en la empresa, provee de un control de ordenes, abonos, y facturas, en caso de ser un superior incluso 
                podras controlara a tus subalternos de una manera agil y sencilla, inicia sesion y descubre todos los beneficios que el Sistema de
                Gestion de Ventas te proveera.
            </p>
            
        </div>
        <div class="col-md-4">
            <h3>Ingresa tus datos</h3>
            <h4>NIT empleado &nbsp;
                <asp:TextBox ID="BoxNIT" runat="server"></asp:TextBox>
            </h4>
            <h4>Contraseña&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="BoxContraseña" runat="server" TextMode="Password"></asp:TextBox>
            </h4>
            
            <p>
                
                <asp:Button ID="Button1" runat="server" CssClass="btn-danger disabled focus" Text="Iniciar Sesion" OnClick="Button1_Click" />
                <asp:Label ID="Advertencia" runat="server" ></asp:Label>
            </p>
            <div>
                <p>
                    Lista vigente:&nbsp;
                    <asp:Label ID="VigenteActual" runat="server" ></asp:Label>
                </p>
            </div>
            <div>
                <p>
                    Vigencia:
                </p>
            </div>
            <div align="Center">

                <asp:Label ID="InicioL" runat="server" ></asp:Label>
&nbsp;-
                <asp:Label ID="FinalL" runat="server"></asp:Label>

            </div>
        </div>
        <div class="col-md-4">
            <h2>Nuevo en la empresa</h2>
            <p>
                Registrate, recuerda que para poder registrarte, necesitas de la autorizacion de un superior.</p>
            <p>
                <a class="btn btn-default" runat ="server" href="~/About">Registro &raquo;</a>
            </p>
            
        </div>

    </div>
    
</asp:Content>
