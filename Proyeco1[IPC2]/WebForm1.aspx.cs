﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Proyeco1_IPC2_
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public static string NITusuario;
        public static int litaActual;
         public string connectionString = @"Data Source=LAPTOP-U0889L30; Initial Catalog = DIPROMA; Integrated Security=True;";
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime Inicio = DateTime.Now;
            DateTime Final = DateTime.Now;
            DateTime Hoy = DateTime.Now;
            int CodigoLista = 0;
            SqlConnection sqlCon1 = new SqlConnection(connectionString);
            sqlCon1.Open();
            SqlCommand comando1 = new SqlCommand("SELECT * FROM LISTAPRECIO", sqlCon1);
            SqlDataReader lector1 = comando1.ExecuteReader();
            while (lector1.Read())
            {
                CodigoLista = lector1.GetInt32(0);
                Inicio = lector1.GetDateTime(1);
                Final = lector1.GetDateTime(2);
                int EsMayorQueLaFechaInicio = DateTime.Compare(Hoy, Inicio);
                int EsMenorQueLaFechaFinal = DateTime.Compare(Hoy, Final);

                if ((EsMayorQueLaFechaInicio >= 0) & (EsMenorQueLaFechaFinal <= 0)) {
                    litaActual = CodigoLista;
                    VigenteActual.Text = litaActual.ToString();
                    InicioL.Text = Inicio.ToString("dd/MM/yyyy");
                    FinalL.Text = Final.ToString("dd/MM/yyyy");
                }               
                
            }

            sqlCon1.Close();
            
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            NITusuario = "";
           
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();

            string nit = BoxNIT.Text;
            string contra = BoxContraseña.Text;
            int TipoEmpleado = 0;

            SqlParameter parnomus = new SqlParameter("@nit", nit);
            SqlParameter parcon = new SqlParameter("@contra", contra);

            SqlCommand comando = new SqlCommand("SELECT NITEmpleado, TipoEmpleado FROM EMPLEADO WHERE NITEmpleado = @nit AND Contraseña = @contra", sqlCon);
            comando.Parameters.Add(parnomus);
            comando.Parameters.Add(parcon);

            SqlDataReader lector = comando.ExecuteReader();
            while (lector.Read()) {
                NITusuario = lector.GetString(0);
                TipoEmpleado = lector.GetInt32(1);
            }

            sqlCon.Close();

            if (NITusuario.Equals(nit))
            {
                Advertencia.Text = "Datos Correctos, eres un " + TipoEmpleado;
                if (TipoEmpleado == 1) {
                    Response.Redirect("VistaPrincipalV.aspx");
                } else if (TipoEmpleado == 2) {
                    Response.Redirect("VistaPrincipalS.aspx");
                } else if (TipoEmpleado == 3) {
                    Response.Redirect("VistaPrincipalG.aspx");
                } else if (TipoEmpleado == 4) {
                    Response.Redirect("AdminGestiones.aspx");
                }
            }
            else {
                Advertencia.Text = "Datos erroneos " ;
            }

        }
    }
}